# Design Patterns in Java

This repository provides a comprehensive overview of Design Patterns in Java from a practical perspective. This repo in particular covers patterns with the use of:
- Use of modern developer tools such as IntelliJ IDEA
- Discussions of pattern variations and alternative approaches
- Use of modern programming approaches: dependency injection, reactive programming and more

This repository provides an overview of all the Gang of Four (GoF) design patterns as outlined in their seminal book, together with modern-day variations, adjustments, discussions of intrinsic use of patterns in the language.

Resources that I've used: 

- Christopher Okhravi's [youtube channel](https://www.youtube.com/channel/UCbF-4yQQAWw-UnuCd2Azfzg). Amazing teacher, hands down.
- [Refactoring.guru](https://refactoring.guru/design-patterns/). Very good and detailed explainations.
- Dmitri Nesteruk's Design Patterns in Java [Udemy course](https://www.udemy.com/course/design-patterns-java/).
- The book ['Dive into Design Patterns'](https://refactoring.guru/design-patterns/book) by Alexander Shvets
