package com.kgenov.DesignPatterns.Creational.Prototype;

import java.util.Arrays;

/*
The Prototype design pattern is all about object copying.

Motivation for using the prototype - complicated objects like cars aren't designed from scratch. If you think that any time
a factory wants to make a new model of a car they do it from scratch, well you'd be mistaken. They'd take an existing design of a car,
change it, improve it and essentially they reiterate existing designs, making them better. The prototype DP is exactly the same approach
in software engineering, so essentially the prototype is an existing object which can be partially or fully constructed.
So, you've already designed something, you've defined a very complicated type, you've initialized it(maybe you used a builder to do so)
and then you simply want to make variations on it. So in order to make variations, you need to make a copy of the prototype and customize it.

To accomplish this we need something called a deep copy - copying not just the object, but all of the object's references. The cloning
needs to be convenient, we have to provide an API, like for example a Factory.
 */
public class Prototype {
    // Empty, using only for file name
}

/*
In the below scenario, we are creating people which have an address and a name. We create a first person which lives on a specific
street, and then a second person who's he's neighbour. We want to avoid instantiating a new object for the second person as it
is pretty similar to the first one. Code e.g. and comments are in the Demo class below.
 */
class Address implements Cloneable {
    public String streetName;
    public int streetNumber;

    public Address(String streetName, int streetNumber) {
        this.streetName = streetName;
        this.streetNumber = streetNumber;
    }

    @Override
    public String toString() {
        return "Address{" +
                "streetName='" + streetName + '\'' +
                ", streetNumber=" + streetNumber +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Address(streetName, streetNumber);
    }
}

class Person implements Cloneable {
    public String[] names;
    public Address address;

    public Person(String[] names, Address address) {
        this.names = names;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "names=" + Arrays.toString(names) +
                ", address=" + address +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Person(names.clone(), (Address) address.clone());
    }
}

class Demo {
    public static void main(String[] args) throws CloneNotSupportedException {
        // We create our first person
        Person nikola = new Person(new String[]{"Nikola", "Kunev"},
                new Address("KunchetoStreet", 69));
        // Suppose we want to create another person which lives next door to Nikola. The question is when we want to make
        // somebody who lives next door - do we have to recreate the large constructor call that we made above?
        // The answer is obviously no, we want ot make a copy of Nikola and modify only the relevant bits.

        // You might be tempted to write something like this:
        Person michael = nikola;
        // And then you can customize Michael by changing his name, assuming they both have the same last names for this e.g.
        michael.names[0] = "Michael";
        michael.address.streetNumber = 70;
        // Now this won't work because of line 73 - we are copying the reference, so Nikola and Michael refer to the same object
        // and as a result they all share the same data. So, when we are overriding Nikola when we assign the name to Michael.
        System.out.println(nikola);
        System.out.println(michael);

        // In order to fix this, I will implement the cloneable interface. (Classes up until this point did not implement the Iface)
        Person constantine = new Person(new String[]{"John", "Constantine"}, new Address("Hell", 420));
        Person chas = (Person) constantine.clone();
        System.out.println(constantine);
        System.out.println(chas);
        // Typically the implementation of Cloneable is not recommended. If you read the documentation, nobody ever tells you
        // you should be implementing cloneable when you want deep copying to be done. Cloneable doesn't really state what the
        // clone method does, and the default behaviour of clone is to actually perform a shallow copy, as opposed to a deep copy.
        // It's better to use other mechanisms, not cloneable to actually do that. This is demonstrated in the next classes of the package.
    }
}