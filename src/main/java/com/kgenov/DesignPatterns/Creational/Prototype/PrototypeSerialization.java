package com.kgenov.DesignPatterns.Creational.Prototype;

import java.io.Serializable;

import org.apache.commons.lang3.SerializationUtils;

/*
One of the big problems with copy constructors is that you would have to build a copy constructor for every single type that you have. If you have a
hierarchy of 20 different types, you would have to make 20 different copy constructors.
Given a complicated hierarchy of types how would you actually copy the objects without implementing copy constructors?
One of the solutions is to use serialization. E.g below
 */
public class PrototypeSerialization {
}

class Foo implements Serializable {
    public String stuff, someOtherStuff;

    public Foo(String stuff, String someOtherStuff) {
        this.stuff = stuff;
        this.someOtherStuff = someOtherStuff;
    }

    @Override
    public String toString() {
        return "Foo{" +
                "stuff='" + stuff + '\'' +
                ", someOtherStuff='" + someOtherStuff + '\'' +
                '}';
    }
}

class DemoSerialization {
    public static void main(String[] args) {
        Foo foo = new Foo("stuff", "otherStuff");
        // Apache commons library for serialization
        Foo foo2 = SerializationUtils.roundtrip(foo); //roundtrip serializes the object and then de-serializes it, making a deep copy
        foo2.someOtherStuff = "This is different now";
        System.out.println(foo);
        System.out.println(foo2);
    }
}