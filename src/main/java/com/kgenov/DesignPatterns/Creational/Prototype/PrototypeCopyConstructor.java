package com.kgenov.DesignPatterns.Prototype;

public class PrototypeCopyConstructor {
}

/*
Once again, we're going to implement an address class and an employee class who once again need to be copiable.
We're gonna do this with copy constructors.
 */
class AddressCC {
    public String streetName, streetNumber, city;

    public AddressCC(String streetName, String streetNumber, String city) {
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.city = city;
    }

    // A copy constructor is a constructor which takes another copy of this particular object,
    // it takes this as an argument and tries to copy each one of this fields. Demonstrated below:
    public AddressCC(AddressCC other) {
        this(other.streetName, other.streetNumber, other.city);
    }

    @Override
    public String toString() {
        return "AddressCC{" +
                "streetName='" + streetName + '\'' +
                ", streetNumber='" + streetNumber + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}

class Employee {
    public String name;
    public AddressCC address;

    public Employee(String name, AddressCC address) {
        this.name = name;
        this.address = address;
    }

    public Employee(Employee other) {
        name = other.name;
        address = other.address;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", address=" + address +
                '}';
    }
}

class DemoCC {
    public static void main(String[] args) {
        Employee john = new Employee("John",
                new AddressCC("Hopkins Boulevard", "120", "London"));
        Employee tom = new Employee(john);

        System.out.println(john + " John hashcode: " + System.identityHashCode(john));
        System.out.println(tom + " Tom hashcode: " + System.identityHashCode(tom));
    }
}