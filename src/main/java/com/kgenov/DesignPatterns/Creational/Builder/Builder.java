package com.kgenov.DesignPatterns.Creational.Builder;
/* konstantin created on 8/3/2020 inside the package - com.kgenov.DesignPatterns.Creational.Builder */

import java.util.ArrayList;
import java.util.Collections;

/*
The builder pattern is a design pattern designed to provide a flexible solution to various object creation problems in OOP.
The intent of the pattern is to seperate the construction of a complex object from it's representation.
Imagine we want to create some functionality which will output HTML elements from our back-end to our web server,
one approach would be to use the JDK's built-in StringBuilder(builder design pattern) to append and format our text to HTML. Below in the main method of the class you'll see the example.
However, with this approach we end up with something which is not convenient, not flexible and hard to maintain. This is why I will create my own builder - HtmlBuilder
 */

public class Builder {
    public static void main(String[] args) {
        // Example of how we can accomplish this with the JDK's StringBuilder
        String hello = "hello";
        StringBuilder sb = new StringBuilder();
        sb.append("<p>")
                .append(hello)
                .append("</p>"); // a builder!
        System.out.println(sb);

        // now we want to build a list with 2 words
        String[] words = {"hello", "world"};
        sb.setLength(0); // clear it
        sb.append("<ul>\n");
        for (String word : words) {
            // indentation management, line breaks and other evils
            sb.append(String.format("  <li>%s</li>\n", word));
        }
        sb.append("</ul>");
        System.out.println(sb);
        // end of example

        // Example of my implementation
        HtmlBuilder builder = new HtmlBuilder("asdas");
        builder.addChild("li", "hello");
        builder.addChild("li", "world");
        System.out.println(builder);


    }
}


class HtmlElement {
    public String name, text;
    public ArrayList<HtmlElement> elements = new ArrayList<>();
    private final int indentSize = 2;
    private final String newLine = System.lineSeparator();

    public HtmlElement() {

    }

    public HtmlElement(String name, String text) {
        this.name = name;
        this.text = text;
    }

    private String toStringImpl(int indent) {
        StringBuilder sb = new StringBuilder();
        String i = String.join("", Collections.nCopies(indent * indentSize, " "));
        sb.append(String.format("%s<%s>%s", i, name, newLine));
        if (text != null && !text.isEmpty()) {
            sb.append(String.join("", Collections.nCopies(indentSize * (indent + 1), " ")))
                    .append(text)
                    .append(newLine);
        }

        for (HtmlElement e : elements)
            sb.append(e.toStringImpl(indent + 1));

        sb.append(String.format("%s</%s>%s", i, name, newLine));
        return sb.toString();
    }

    @Override
    public String toString() {
        return toStringImpl(0);
    }
}

class HtmlBuilder{
    private String rootName;
    private HtmlElement root = new HtmlElement();

    public HtmlBuilder(String rootName) {
        this.rootName = rootName;
        root.name = rootName;
    }

    public void addChild(String childName, String childText){
        HtmlElement element = new HtmlElement(childName, childText);
        root.elements.add(element);
    }

    public void clear(){
        root = new HtmlElement();
        root.name = rootName;
    }

    @Override
    public String toString() {
        return root.toString();
    }
}
