package com.kgenov.DesignPatterns.Factory;

/*
Factory method is a creational design pattern which solves the problem of creating product objects without specifying their concrete classes.
Factory Method defines a method, which should be used for creating objects instead of direct constructor call (new operator).
Subclasses can override this method to change the class of objects that will be created.
The Factory Method pattern is widely used in Java code. It’s very useful when you need to provide a high level of flexibility for your code.

In the e.g. Point class below, we want to be able to create a new point from one of 2 coordinate systems. However, taking the normal approach of overloading constructors
will not work in Java, as we cannot have 2 constructors with the same number of arguments & same variable type;
 */
public class FactoryMethod {
}

// 1. Start code example from here.
class PointWithoutDP {
    private double x, y;

    // Cartesian system constructor
    public PointWithoutDP(double x, double y) {
        this.x = x;
        this.y = y;
    }

    // Java does not allow us to create a second constructor with the same number of arguments and data types
//    public PointWithoutDP(double rho, double theta) { // Polar system constructor
//        x = rho * Math.cos(theta);
//        y = rho * Math.sin(theta);
//    }

    // One approach would be to add an enum class for the coordinate systems and apply the calculations based on what CS we got as an argument
    public PointWithoutDP(double a, double b, CoordinateSystem cs) {
        switch (cs) {
            case CARTESIAN:
                this.x = a;
                this.y = b;
                break;
            case POLAR:
                x = a * Math.cos(b);
                y = a * Math.sin(b);
        }
    }

    // With this approach our code is messy. We can easily confuse the user, and if we'd want to add more point systems the code will be a suboptimal.
    // This is where we can introduce the Factory Method DP. We will not allow the user to call our constructor, but instead use static factory methods
    // which allow us to separate the creational logic, avoid confusing the user and easily maintain/extend our code. (Refer to the Point class below)


}

// enum used for the above example
enum CoordinateSystem {
    CARTESIAN,
    POLAR
}

// =========================================================================

// 2. This is the correct way to design our code
// Factory Method approach:
class PointWithDP {
    private double x, y;

    // Do not allow access to the constructor as object creation is handled by static factory methods
    private PointWithDP(double x, double y) {
        this.x = x;
        this.y = y;
    }

    // Now the creational logic is handled by the 2 static methods
    public static PointWithDP newCartesianPoint(double x, double y) {
        return new PointWithDP(x, y);
    }

    public static PointWithDP newPolarPoint(double rho, double theta) {
        return new PointWithDP(rho * Math.cos(theta), rho * Math.sin(theta));
    }
    // Clean code achieved : )

    public static void main(String[] args) {
        PointWithDP cartesianPoint = PointWithDP.newCartesianPoint(10, 5);
        PointWithDP polarPoint = PointWithDP.newPolarPoint(5, 5);
    }
}
