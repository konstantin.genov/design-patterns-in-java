package com.kgenov.DesignPatterns.Creational.Factory;

/*
 When you end up with a lot of factory methods, it is best to implement a factory that holds all of them.
 */
public class Factory {
}

class Point {
    private double x, y;

    private Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static class Factory {
        public static Point newCartesianPoint(double x, double y) {
            return new Point(x, y);
        }

        public static Point newPolarPoint(double rho, double theta) {
            return new Point(rho * Math.cos(theta), rho * Math.sin(theta));
        }
    }

    public static void main(String[] args) {
        Point polarPoint = Point.Factory.newPolarPoint(6, 9);
    }

}
