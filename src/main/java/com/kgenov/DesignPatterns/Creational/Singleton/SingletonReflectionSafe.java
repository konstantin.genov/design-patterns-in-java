package com.kgenov.DesignPatterns.Singleton;

/*
Another approach to make the singleton reflection safe with serialization available (not like with enum based)
This is however without a downside, as we have to throw an exception to accomplish this.
 */
public class SingletonReflectionSafe {
    //empty, used for file name
}

class SingletonClass {

    private static SingletonClass sSoleInstance;

    //private constructor.
    private SingletonClass() {

        //Prevent form the reflection api.
        if (sSoleInstance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public static SingletonClass getInstance() {
        if (sSoleInstance == null) { //if there is no instance available... create new one
            sSoleInstance = new SingletonClass();
        }

        return sSoleInstance;
    }

    // prevent serialization instantiation
    Object readResolve() {
        return sSoleInstance;
    }
}