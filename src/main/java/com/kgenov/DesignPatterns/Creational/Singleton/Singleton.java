package com.kgenov.DesignPatterns.Singleton;

import java.io.*;

/*
Singleton pattern is one of the simplest design patterns in Java. This type of design pattern comes under creational pattern as this pattern provides one of the best ways to create a SINGLE object.
This pattern involves a single class which is responsible to create an object while making sure that only single object gets created.
This is useful when exactly one object is needed to coordinate actions across the system.

The implementation of Java Singleton pattern has always been a controversial topic among developers.
Critics consider the singleton to be an anti-pattern in that it is frequently used in scenarios where it is not beneficial,
introduces unnecessary restrictions in situations where a sole instance of a class is not actually required, and introduces global state into an application.

Even Erich Gamma(GoF member) stated that the book authors had a discussion in 2005 on how they would have refactored the book and concluded that they would have
recategorized some patterns and added a few additional ones. Gamma wanted to remove the Singleton pattern, but there was no consensus among the authors to do so.

Singletons in Java can also be instantiated more than once through the use of reflection or serialization. Below I've show a way to counter this serialization hack
through adding the object readResolve method. Later on I will show how we can defeat reflection through the use of enums.
 */
public class Singleton {
}


class BasicSingleton implements Serializable {
    // cannot new this class, however
    // * instance can be created deliberately (reflection)
    // * instance can be created accidentally (serialization)
    private BasicSingleton() {
        System.out.println("Singleton is initializing");
    }

    private static final BasicSingleton INSTANCE = new BasicSingleton();

    private int value = 0;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    // required for correct serialization
    // readResolve is used for _replacing_ the object read from the stream

//  protected Object readResolve()
//  {
//    return INSTANCE;
//  }

    // generated getter
    public static BasicSingleton getInstance() {
        return INSTANCE;
    }
}

class BasicSingletonDemo {
    static void saveToFile(BasicSingleton singleton, String filename)
            throws Exception {
        try (FileOutputStream fileOut = new FileOutputStream(filename);
             ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
            out.writeObject(singleton);
        }
    }

    static BasicSingleton readFromFile(String filename)
            throws Exception {
        try (FileInputStream fileIn = new FileInputStream(filename);
             ObjectInputStream in = new ObjectInputStream(fileIn)) {
            return (BasicSingleton) in.readObject();
        }
    }

    public static void main(String[] args) throws Exception {
        BasicSingleton singleton = BasicSingleton.getInstance();
        singleton.setValue(111);

        String filename = "singleton.bin";
        saveToFile(singleton, filename);

        singleton.setValue(222);

        BasicSingleton singleton2 = readFromFile(filename);

        System.out.println(singleton == singleton2);

        System.out.println(singleton.getValue());
        System.out.println(singleton2.getValue());
    }
}