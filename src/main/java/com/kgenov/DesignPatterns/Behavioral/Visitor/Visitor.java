package com.kgenov.DesignPatterns.Behavioral.Visitor;
/* konstantin created on 11/4/2020 inside the package - com.kgenov.DesignPatterns.Behavioral.Visitor */

/*
The visitor pattern is a pattern where one interface (Visitor) defines a computation / operation and another (Visitable) is responsible for providing data access.

It helps your code adhere to the single responsibility principle as if a class is just responsible for holding data,
it does not clutter it up with adding computation or additional logic to it and any additional operations can be added externally in the Visitor class.

The reason to use it is given in Head First Design Patterns as:
Use when you want to add capabilities to a composite of objects and encapsulation is not important.

The advantage of using the visitor DP instead of simply extending a class is that it can handle different data types through method overloading.
Annoyingly though, every time a new data type has to be handled it needs to be added to the visitable interface, which is an OCP violation.
But if you know there is a definite, relatively small number of data types this might not be such a problem.

If you are at a point where you have to break the open closed principle to add new methods to an already used, existing class, the best way is to implement a visitor.
By doing this, you have to modify the existing class only once and then you can supply the new methods by using different visitors, instead of adding new and new methods to your class.

Let's say you have different credit cards which provide a cashback opportunity for different services. Usually, you'd need to add new methods for each new service which you
introduce as a cashback opportunity, making your code very inconvenient, and in time impossible to change & scale. By introducing a visitor which introduces the requested
method each time it is needed we avoid this severe violation of the OCP.

This scenario is demonstrated in the code below.
 */

public class Visitor {
}

//visitable
interface CreditCard {
    String getName();

    void accept(OfferVisitor visitor); // visitor method
}

//visitor
interface OfferVisitor {
    void visitBronzeCreditCard(BronzeCreditCard card);

    void visitSilverCreditCard(SilverCreditCard card);

    void visitGoldCreditCard(GoldCreditCard card);

}

class BronzeCreditCard implements CreditCard {

    @Override
    public String getName() {
        return "Bronze credit card.";
    }

    @Override
    public void accept(OfferVisitor visitor) {
        visitor.visitBronzeCreditCard(this);
    }
}

class SilverCreditCard implements CreditCard {

    @Override
    public String getName() {
        return "Silver credit card.";
    }

    @Override
    public void accept(OfferVisitor visitor) {
        visitor.visitSilverCreditCard(this);
    }
}

class GoldCreditCard implements CreditCard {

    @Override
    public String getName() {
        return "Gold credit card.";
    }

    @Override
    public void accept(OfferVisitor visitor) {
        visitor.visitGoldCreditCard(this);
    }
}

// now we can create concrete OfferVisitors, each having different interactions with the different cards
class AirtravelOfferlVisitor implements OfferVisitor {

    @Override
    public void visitBronzeCreditCard(BronzeCreditCard card) {
        System.out.println("The Bronze credit card has 1% cashback on air-travel purchases.");
    }

    @Override
    public void visitSilverCreditCard(SilverCreditCard card) {
        System.out.println("The Silver credit card has 2.5% cashback on air-travel purchases.");

    }

    @Override
    public void visitGoldCreditCard(GoldCreditCard card) {
        System.out.println("The Gold credit card has 5.2% cashback on air-travel purchases.");

    }
}

class FoodOfferVisitor implements OfferVisitor {

    @Override
    public void visitBronzeCreditCard(BronzeCreditCard card) {
        System.out.println("The Bronze credit card has 0.1% cashback for food supplies.");
    }

    @Override
    public void visitSilverCreditCard(SilverCreditCard card) {
        System.out.println("The Silver credit card has 0.3% cashback for food supplies.");

    }

    @Override
    public void visitGoldCreditCard(GoldCreditCard card) {
        System.out.println("The Gold credit card has 0.6% cashback for food supplies.");

    }
}

class VisitorDemo {
    public static void main(String[] args) {
        // Scenario: we've had a concrete credit card class and we needed new methods for air travel and food supplies cashback calculation. Instead of adding those 2 methods to the interface, and then implementing them
        // in each sub-class, we simply defined only 1 method in the CreditCard interface - accept() which takes on a visitor who supplies the functionality we need. Thus, we had to violate the OCP in the CreditCard
        // interface and implementations **only** once, and have opened our hierarchy to future behavioural extension.
        CreditCard bronze = new BronzeCreditCard();
        CreditCard silver = new SilverCreditCard();
        CreditCard gold = new GoldCreditCard();

        // we only need to create 1 visitor which can interact with our 3 objects
        OfferVisitor airtravelVisitor = new AirtravelOfferlVisitor();

        bronze.accept(airtravelVisitor);
        silver.accept(airtravelVisitor);
        gold.accept(airtravelVisitor);

        OfferVisitor foodVisitor = new FoodOfferVisitor();

        gold.accept(foodVisitor);

    }
}