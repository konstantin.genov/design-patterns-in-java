package com.kgenov.DesignPatterns.Behavioral.ChainOfResponsibility;
/* konstantin created on 10/27/2020 inside the package - com.kgenov.DesignPatterns.Behavioral.ChainOfResponsibility */
/*
Chain of responsibility pattern is used to achieve loose coupling in software design where a request from the client is passed to a chain of objects to process them.
Later, the object in the chain will decide themselves who will be processing the request and whether the request is required to be sent to the next object in the chain or not.
 */
public class ChainOfResponsibility {
    //empty, used for filename
}
// Simple and popular example of CoR -- method chain

class Creature {
    public String name;
    public int attack, defense;

    public Creature(String name, int attack, int defense) {
        this.name = name;
        this.attack = attack;
        this.defense = defense;
    }

    @Override
    public String toString() {
        return "Creature{" +
                "name='" + name + '\'' +
                ", attack=" + attack +
                ", defense=" + defense +
                '}';
    }
}

// Let's suppose the creature goes around and keeps finding magic objects which approve it's attack & defense values, or maybe it encounters a witch
// which casts some sort of spell that it increases or decreases those values.

class CreatureModifier{
    // store reference to the creature we are modifying
    protected Creature creature;
    // keep in mind that creature modifiers can be chained together, so we can have several ones being applied to the same creature
    protected CreatureModifier next; // create chain

    public CreatureModifier(Creature creature) {
        this.creature = creature;
    }
    public void add(CreatureModifier creatureModifier){
        // recursive call throughout the chain
        if (next != null){
            next.add(creatureModifier);
        } else {
            next = creatureModifier;
        }
    }

    public void handle(){
        if (next != null){
            next.handle();
        }
    }
}

class DoubleAttackModifier extends CreatureModifier{

    public DoubleAttackModifier(Creature creature) {
        super(creature);
    }

    @Override
    public void handle() {
        System.out.println("Doubling " + creature.name + "'s attack!");
        creature.attack *= 2;
        super.handle(); // build chain
    }
}

class IncreaseDefenseModifier extends CreatureModifier {

    public IncreaseDefenseModifier(Creature creature) {
        super(creature);
    }

    @Override
    public void handle() {
        System.out.println("Increasing " + creature.name + "'s defense!");
        creature.defense += 3;
        super.handle();
    }
}
//creature got cursed
class NoBonusesModifier extends CreatureModifier{
    public NoBonusesModifier(Creature creature) {
        super(creature);
    }

    @Override
    public void handle() {
        System.out.println("No modifiers can be applied. Creature " + creature.name + " got cursed!");
        // we don't make a call to super.handle(), this way we do not start the chain at all
    }
}

class CreatureDemo {
    public static void main(String[] args) {
        Creature theElder = new Creature("The Elder", 50, 40);
        System.out.println(theElder);
        CreatureModifier root = new CreatureModifier(theElder); //starting point for traversal
        root.add(new DoubleAttackModifier(theElder));
        root.add(new IncreaseDefenseModifier(theElder));
        root.handle(); //start recursive call to apply modifiers

        System.out.println(theElder);

        Creature theShaper = new Creature("The Shaper", 45, 25);
        System.out.println(theShaper);
        CreatureModifier shaperRoot = new CreatureModifier(theShaper);
        // shaper gets cursed
        shaperRoot.add(new NoBonusesModifier(theShaper));
        shaperRoot.add(new DoubleAttackModifier(theShaper));
        shaperRoot.handle();
        System.out.println(theShaper);
    }
}