package com.kgenov.DesignPatterns.Behavioral.NullObject;
/* konstantin created on 10/29/2020 inside the package - com.kgenov.DesignPatterns.Behavioral.NullObject */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
In most object-oriented languages, such as Java or C#, references may be null.
These references need to be checked to ensure they are not null before invoking any methods, because methods typically cannot be invoked on null references.

Instead of using a null reference to convey absence of an object (for instance, a non-existent customer), one uses an object which implements the expected interface,
but whose method body is empty. The advantage of this approach over a working default implementation is that a null object is very predictable and has no side effects: it does nothing.

For example, a function may retrieve a list of files in a folder and perform some action on each. In the case of an empty folder, one response may be to throw an exception or return a null reference rather than a list.
Thus, the code which expects a list must verify that it in fact has one before continuing, which can complicate the design.
By returning a null object (i.e. an empty list) instead, there is no need to verify that the return value is in fact a list.
The calling function may simply iterate the list as normal, effectively doing nothing. It is, however, still possible to check whether the return value is a null object (an empty list) and react differently if desired.
The null object pattern can also be used to act as a stub for testing, if a certain feature such as a database is not available for testing.

Sometimes some methods return null instead of real object, you will have many checks for null in your code:
if (customer == null) {
  plan = BillingPlan.basic();
}
else {
  plan = customer.getPlan();
}
===============
You can see how this can become very troublesome if we have to check every time if a customer object is null.

So instead of null, we return a null object that exhibits the default behaviour:
class NullCustomer extends Customer {
  boolean isNull() {
    return true;
  }
  Plan getPlan() {
    return new NullPlan();
  }
customer = (order.customer != null) ?
  order.customer : new NullCustomer();

plan = customer.getPlan();
 */
public class NullObject {
    //empty, used for filename
}

// Imagine we have a database with customers and someone queries it, searching for customers by name. Some customers will not exist, so I'll treat them as a NullCustomer, instead of performing null checks.
interface ICustomer {
    boolean isNull();

    String getName();

}

class Customer implements ICustomer {

    private String name;

    public Customer(String name) {
        this.name = name;
    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        return this.name;
    }
}

class NullCustomer implements ICustomer {

    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public String getName() {
        return "Customer is not available";
    }
}

class CustomerFactory {
    private static List<String> realCustomers = new ArrayList<>(Arrays.asList("John", "Samantha", "Tealc"));

    public static ICustomer findCustomer(String name){
        for (String customer : realCustomers
             ) {
            if (customer.equals(name)){
                return new Customer(name);
            }
        }
        return new NullCustomer();
    }
}

class Demo {

    public static void main(String[] args) {
        // fetching customers which exist in database
        ICustomer john = CustomerFactory.findCustomer("John");
        System.out.println(john.getName());
        ICustomer tealc = CustomerFactory.findCustomer("Tealc");
        System.out.println(tealc.getName());

        // fetch customer who doesn't exist; without NOP would result in null pointer, or we'd have to do checks
        ICustomer daniel = CustomerFactory.findCustomer("Daniel Jackson");
        System.out.println(daniel.getName());

    }
}
