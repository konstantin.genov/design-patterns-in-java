package com.kgenov.DesignPatterns.Behavioral.State;

/* konstantin created on 10/31/2020 inside the package - com.kgenov.DesignPatterns.Behavioral.State */
/*
State is a behavioral design pattern that lets an object alter its behavior when its internal state changes. It appears as if the object changed its class.

The State pattern is closely related to the concept of a Finite-State Machine - https://en.wikipedia.org/wiki/Finite-state_machine

The main idea is that, at any given moment, there’s a finite number of states which a program can be in. Within any unique state, the program behaves differently,
and the program can be switched from one state to another instantaneously. However, depending on a current state, the program may or may not switch to certain other states.
These switching rules, called transitions, are also finite and predetermined, meaning you need to think of all of the possible scenarios between different states and their
reactions to one another.

You can also apply this approach to objects. Imagine that we have a Turnstile class. (the gates at metro stations which open once you pay)
A turnstile can be in one of three states: Open, Closed and Processing(payments, etc.). The enter method of the turnstile works a little bit differently in each state:
- In OpenState, it allows a person to pass
- In ClosedState, it doesn't allow a person to pass until they pay
- In ProcessingState, the gate remains in it's current state until the processing of the payment is complete

Solution:
The State pattern suggests that you create new classes for all possible states of an object and extract all state-specific behaviors into these classes.
Instead of implementing all behaviors on its own, the original object, called context,
stores a reference to one of the state objects that represents its current state, and delegates all the state-related work to that object.
 */
public class State {
    //empty, used for filename
}

// called Context
class Turnstile {
    private TurnstileState state;

    //always instance the class with an initial state - closed
    public Turnstile() {
        this.state = new ClosedTurnstileState(this);
    }

    public void enter() {
        this.state.enter();
    }

    public void pay() {
        this.state.pay();
    }

    public void payOk() {
        this.state.payOk();
    }

    public void payFailed() {
        this.state.payFailed();
    }

    public void changeState(TurnstileState state) {
        this.state = state;
    }
}

// called State
interface TurnstileState {
    void enter();

    void pay();

    void payOk();

    void payFailed();
}

// concrete State
class OpenTurnstileState implements TurnstileState {
    private Turnstile turnstile;

    public OpenTurnstileState(Turnstile turnstile) {
        this.turnstile = turnstile;
    }

    @Override
    public void enter() {
        System.out.println("You went through the turnstile.");
        this.turnstile.changeState(new ClosedTurnstileState(this.turnstile));
    }

    @Override
    public void pay() {
        System.out.println("Gate is already open. Please proceed.");
    }

    @Override
    public void payOk() {
        this.pay();
    }

    @Override
    public void payFailed() {
        this.pay();
    }
}

class ClosedTurnstileState implements TurnstileState {
    private Turnstile turnstile;

    public ClosedTurnstileState(Turnstile turnstile) {
        this.turnstile = turnstile;
    }

    @Override
    public void enter() {
        System.out.println("Turnstile is closed. Please purchase a ticket.");
    }

    @Override
    public void pay() {
        System.out.println("Processing payment...");
        this.turnstile.changeState(new ProcessingTurnstileState(this.turnstile));
        this.turnstile.pay();
    }

    @Override
    public void payOk() {
        this.pay();
    }

    @Override
    public void payFailed() {
        this.pay();
    }
}

class ProcessingTurnstileState implements TurnstileState {
    private Turnstile turnstile;

    public ProcessingTurnstileState(Turnstile turnstile) {
        this.turnstile = turnstile;
    }

    @Override
    public void enter() {
        System.out.println("Please wait, payment is still being processed.");

    }

    @Override
    public void pay() {
        //simulate payment functionality; no need to implement as this is just an overview of the pattern
        if (true) {
            payOk();
        } else {
            payFailed();
        }
    }

    @Override
    public void payOk() {
        System.out.println("Payment successful. You can now proceed.");
        this.turnstile.changeState(new OpenTurnstileState(this.turnstile));
    }

    @Override
    public void payFailed() {
        System.out.println("Payment unsuccessful. Please, try again.");
        this.turnstile.changeState(new ClosedTurnstileState(this.turnstile));
    }
}

class StateDemo {
    public static void main(String[] args) {
        Turnstile turnstile = new Turnstile();
        turnstile.enter();
        turnstile.pay();
    }
}