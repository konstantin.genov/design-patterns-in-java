package com.kgenov.DesignPatterns.Behavioral.TemplateMethod;

import java.util.regex.Pattern;

/* konstantin created on 11/1/2020 inside the package - com.kgenov.DesignPatterns.Behavioral.TemplateMethod */
/*
The template method design pattern is to define an algorithm as a skeleton of operations and leave the details to be implemented by the child classes.
The overall structure and sequence of the algorithm is preserved by the parent class. Template means Preset format like HTML templates which has a fixed preset format.
Similarly in the template method pattern, we have a preset structure method called template method which consists of steps.
This steps can be an abstract method which will be implemented by its subclasses.

Let's imagine you want to create a login template for your website where you can log on using different services like Google, Facebook, etc.
You'd define the general steps of the login which are all shared between different implementations. Then declare specific abstract steps which have to be
implemented/overridden by subclasses to complete the algorithm. The login process in the abstract class is not complete, you need to define concrete classes which
supply the code for the missing pieces.

Pseudo code:
abstract class AuthenticateUser{
....
    public void authenticate(){
operation1(username, password);
do more logic..
operation2();
more logic..
}

abstract void operation1(String username, String password);
abstract void operation2();
// end of class
}
Then you'd have concrete class implementations which extend the abstract class, provide logic for operation1 & operation2, then you'd call the authenticate method
on the instantiated object of the concrete class -> facebookAuthentication.authenticate()
=====
*** The template method pattern is built around inheritance, so in my opinion it is inherently dangerous to use. Unless, you have a scenario which is SUPER appropriate
for the template method pattern. If you know that the structure of your algorithm will always be invariant/fixed,
the steps of the algorithm will NEVER change, then use it by all means. If you have concerns about whether that is true or not, maybe template pattern is not the answer.
 */
public class TemplateMethod {
}

// simulate a record being saved to a DB
abstract class Record {
    public void save() {
        System.out.println("Saving record...");
        this.validate(); //delegate to subclass instance
        System.out.println("Information validated..");
        this.beforeSave(); //optional functionality before saving
        System.out.println("Successfully saved record to database.");
    }

    public abstract void validate();

    // virtual method, used as hook
    public void beforeSave() {
        // no default implementation, and does not have to be implemented for save() to work, acts as hook
    }
}

// in this class we are not implementing the beforeSave() hook
class User extends Record {
    private String userName;

    public User(String userName) {
        this.userName = userName;
    }

    // digits in username not allowed
    @Override
    public void validate() {
        String regex = "(.)*(\\d)(.)*";
        Pattern pattern = Pattern.compile(regex);
        boolean containsNumber = pattern.matcher(this.userName).matches();
        if (containsNumber) {
            System.out.println("Username " + userName + " contains a digit and is invalid. Please, change your username.");
            System.exit(1);
        } else
            System.out.println("Username " + userName + " successfully validated");
    }

    public String getUserName() {
        return userName;
    }
}

// implementing hook here
class Comment extends Record {
    private User user;
    private String comment;

    public Comment(User user, String comment) {
        this.user = user;
        this.comment = comment;
    }

    //pseudo functionality; comments cannot be all uppercase
    @Override
    public void validate() {
        if (this.comment.toUpperCase().equals(this.comment)) {
            System.out.println("You can't use uppercase in your entire comment.");
            System.exit(1);
        } else {
            System.out.println("Comment validated.");
        }
    }

    //pseudo beforeSave functionality
    @Override
    public void beforeSave() {
        System.out.println("Checking for available database agent..");
        System.out.println("Database agent found.");
    }
}

class TemplateDemo {
    public static void main(String[] args) {
        User validUser = new User("Carnivore");
        User invalidUser = new User("Carnivorous666"); //contains digit, thus invalid

        validUser.save(); // successful
//        invalidUser.save(); // unsuccessful.. exit code 1
        System.out.println("\n =============== \n");

        Comment validComment = new Comment(validUser, "Hi.");
        Comment invalidComment = new Comment(validUser, "HELLO");

        validComment.save(); //successful
        System.out.println("\n =============== \n");
        invalidComment.save();
    }
}
