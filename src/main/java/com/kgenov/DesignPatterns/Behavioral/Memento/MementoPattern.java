package com.kgenov.DesignPatterns.Behavioral.Memento;

import java.util.ArrayList;
import java.util.List;

/* konstantin created on 10/28/2020 inside the package - com.kgenov.DesignPatterns.Behavioral.Memento */
/*
The Memento Design Pattern offers a solution to implement undoable actions. We can do this by saving the state of an object at a given instant and restoring it
if the actions performed since need to be undone. Practically, the object whose state needs to be saved is called an Originator.
The Caretaker is the object triggering the save and restore of the state, which is called the Memento.
The Memento object should expose as little information as possible to the Caretaker. This is to ensure that we don't expose the internal state of the Originator
to the outside world, as it would break encapsulation principles. However, the Originator should access enough information in order to restore to the original state.

Intent:

• Without violating encapsulation, capture and externalize an object's internal state so that the object can be returned to this state later.
• A magic cookie that encapsulates a "check point" capability.
• Promote undo or rollback to full object status.

 */
public class MementoPattern {
    //empty, used for filename
}

// Memento/Token
class Memento {
    private double balance;

    public Memento(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }
}

// The Originator
// We can store the states in separate Memento objects or we can use the caretaker class to add each state to our list
class BankAccount {
    private double balance;
    private String accountHolder;

    public BankAccount(double balance, String accountHolder) {
        this.balance = balance;
        this.accountHolder = accountHolder;
    }

    public Memento desposit(double amount) {
        this.balance += amount;
        return new Memento(balance);
    }

    public void restore(Memento m) {
        this.balance = m.getBalance();
    }

    public double getAmount() {
        return balance;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    @Override
    public String toString() {
        return accountHolder + "'s account has a balance of: " + balance;
    }
}

// Approach with caretaker
class CareTaker {
    private List<Memento> states = new ArrayList<>();

    public void addState(Memento state) {
        states.add(state);
    }

    public Memento getStateFromMemento(int i) {
        if (i > states.size() - 1) {
            System.out.println("There's no Memento at index " + i + ". Rolling back to last known Memento");
            return states.get(states.size() - 2);
        } else {
            return states.get(i);
        }

    }

}

class MementoDemo {
    public static void main(String[] args) {
        // Without the use of a caretaker
        BankAccount account = new BankAccount(200, "Astra Zeneca");

        Memento start = new Memento(account.getAmount());
        System.out.println("Account before deposits. " + account);

        Memento m1 = account.desposit(50);
        System.out.println("Deposit 50 " + account);

        Memento m2 = account.desposit(100);
        System.out.println("Deposit 100 " + account);
        // restoring without use of the care taker
        account.restore(start);
        System.out.println(account);
        System.out.println("------End of Account 1 operations------");

        // With the use of a caretaker
        BankAccount account2 = new BankAccount(500, "Pfizer");
        CareTaker careTaker = new CareTaker();
        careTaker.addState(new Memento(account2.getAmount())); // array list based - at index 0
        System.out.println("Account " + account2.getAccountHolder() + "'s balance before deposit: " + account2.getAmount());

        careTaker.addState(account2.desposit(50));
        System.out.println("Deposit 50 " + account2);

        careTaker.addState(account2.desposit(69));
        System.out.println("Deposit 69 " + account2);

        account2.restore(careTaker.getStateFromMemento(3)); //index 1 -> deposit 50; total 550
        System.out.println("Account after restoring: " + account2);

    }
}
