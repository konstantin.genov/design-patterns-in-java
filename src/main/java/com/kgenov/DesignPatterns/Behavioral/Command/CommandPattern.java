package com.kgenov.DesignPatterns.Behavioral.Command;
/* konstantin created on 10/27/2020 inside the package - com.kgenov.DesignPatterns.Behavioral.Command */

/*
Command is a behavioral design pattern that turns a request into a stand-alone object that contains all information about the request.
This transformation lets you parameterize methods with different requests, delay or queue a request’s execution, and support undoable operations.
It also allows us to undo an execution of a command. Think of a software like Photoshop - you have a list of commands performed on your image, you can undo and re-do those commands.

Suppose you are building a home automation system.
There is a programmable remote which can be used to turn on and off various items in your home like lights, stereo, AC etc.
A good example are the Philips HUE smart lights. You can program each button on the remote to send a specific command by using Philips' app. In sense of code,
this means that we can swap in/out different commands at runtime.

Let's get back to our supposed scenario. As smart homes become more popular, a lot more devices with smart capabilities are being introduced each day.
So, you've bought this smart system, got the remote and you only have 2 smart devices which you can interface with the smart programmable remote.
Below we will simulate this entire process - of you using the smart app and adding/swapping additional functionality.

 */
public class CommandPattern {
    // empty, used for filename
}

// interface for all future commands
interface Command {
    void execute();

    void unexecute();
}

// create the smart lights in our house

class Light {
    private String lightLocation;

    public Light(String lightLocation) {
        this.lightLocation = lightLocation;
    }

    public void on() {
        System.out.printf("Light in %s switched on.\n", lightLocation);
    }

    public void off() {
        System.out.printf("Light in %s switched off.\n", lightLocation);
    }
}

// commands for the light
class LightOnCommand implements Command {
    // aggregate
    private Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        this.light.on();
    }

    @Override
    public void unexecute() {
        this.light.off();
    }
}

class LightOffCommand implements Command {
    // aggregate
    private Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        this.light.off();
    }

    @Override
    public void unexecute() {
        this.light.on();
    }
}

// now we create a stereo;
class Stereo {
    // when you turn on a stereo system, a few things happen - it goes on, the CD gets loaded and the volume is established
    public void on() {
        System.out.println("Stereo is on");
    }

    public void off() {
        System.out.println("Stereo is off");
    }

    public void setCD() {
        System.out.println("Stereo is set " +
                "for CD input");
    }

    public void setDVD() {
        System.out.println("Stereo is set" +
                " for DVD input");
    }

    public void setRadio() {
        System.out.println("Stereo is set" +
                " for Radio");
    }

    public void setVolume(int volume) {
        System.out.println("Stereo volume set"
                + " to " + volume);
    }
}

class StereoOnCommand implements Command {
    //aggregate
    Stereo stereo;

    public StereoOnCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    //when a stereo is turned out, several things happen at once
    @Override
    public void execute() {
        this.stereo.on();
        this.stereo.setCD();
        this.stereo.setVolume(69);
    }

    @Override
    public void unexecute() {
        this.stereo.off();
    }
}

class StereoOffCommand implements Command {
    //aggregate
    Stereo stereo;

    public StereoOffCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    @Override
    public void execute() {
        this.stereo.off();
    }

    @Override
    public void unexecute() {
        StereoOnCommand stereoOnCommand = new StereoOnCommand(stereo);
        stereoOnCommand.execute();
    }
}

// Invoker
//remote control with only 1 button
class SimpleRemote {
    Command button;

    public SimpleRemote() {
    }

    //inject command to button
    public void setCommand(Command command) {
        this.button = command;
    }

    // when you click the button, execute said command
    public void shortButtonPress() {
        this.button.execute();
    }

    // hold the button to undo command
    public void longButtonPress() {
        this.button.unexecute();
    }
}

class CommandDemo {
    public static void main(String[] args) {
        //create our smart devices
        SimpleRemote remote = new SimpleRemote();
        Light light = new Light("living room");
        Stereo stereo = new Stereo();

        // let's first program our button to use the stereo
        remote.setCommand(new StereoOnCommand(stereo));
        remote.shortButtonPress(); // turn it on
        remote.longButtonPress(); // revert command

        // now we use the app on our phone to set the light as our receiver
        // see how we can swap receivers at run time?
        remote.setCommand(new LightOnCommand(light));
        remote.shortButtonPress();

    }
}


