package com.kgenov.DesignPatterns.Behavioral.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/* konstantin created on 10/29/2020 inside the package - com.kgenov.DesignPatterns.Behavioral.Observer */
/*
Observer is a behavioral design pattern that lets you define a subscription mechanism to notify multiple objects about any events that happen to the object they’re observing.

Imagine that you have two types of objects: a weather station and a weather app which fetches that information.
The weather app is very interested in lots of weather conditions which change fairly regularly.

The app queries/asks the station every 10 minutes and check weather conditions, but during those periods the app won't have the latest prognosis.
One approach would be to make the weather app query the station every 5 seconds, but that'd be a heavy operation and it might put strain on the system of the station,
especially if other weather apps start doing this. The best solution would be to create a publish/subscribe model - every time the weather station has new data
it will push a notification/update to all of the apps and inform them of the changes.
The observer pattern has a one to many relationship, meaning many objects observe one object referred to as the subject.
In our case, the weather station is the subject (observable) and the weather apps are the observers.
 */
public class Observer {
}

// implementing it myself without using built in Java abstractions
class WeatherStation{
    private double temperature;
    private List<MobileApp> observers = new ArrayList<>();

    public void addObserver(MobileApp observer){
        observers.add(observer);
    }

    public void removeObserver(MobileApp observer){
        observers.remove(observer);
    }

    public void setWeather(double temperature){
        this.temperature = temperature;

        for (MobileApp app : observers
             ) {
            app.update(this.temperature);
        }
    }
}
// observer interface
interface MobileApp {
    void update(double temperature);
}

class WeatherApplication implements MobileApp {
    private double temperature;
    private String applicationName;

    public WeatherApplication(String applicationName) {
        this.applicationName = applicationName;
    }

    @Override
    public void update(double temperature) {
        this.setTemperature(temperature);
    }

    public double getTemperature() {
        return temperature;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public void displayWeather() {
        System.out.println(this.getApplicationName() + " reports the current temperature is " + this.getTemperature() + " degrees celsius.");
    }
}

class WeatherDemo {
    public static void main(String[] args) {
        WeatherStation weatherStation = new WeatherStation();
        WeatherApplication app1 = new WeatherApplication("The Weather Channel");
        WeatherApplication app2 = new WeatherApplication("National News");

        // add observers to the station
        weatherStation.addObserver(app1);
        weatherStation.addObserver(app2);
        // set weather
        weatherStation.setWeather(16);
        app1.displayWeather();
        app2.displayWeather();
        // weather changes
        weatherStation.setWeather(18);
        app1.displayWeather();
        app2.displayWeather();
    }
}

// this is an implementation using Java's built in functionality

class OWeatherStation extends java.util.Observable {
    private double temperature;

    public void setWeather(double temperature) {
        this.temperature = temperature;
        setChanged();
        notifyObservers(temperature);
    }

}

class OWeatherApplication implements java.util.Observer {
    private double temperature;
    private String applicationName;

    public OWeatherApplication(String applicationName) {
        this.applicationName = applicationName;
    }

    @Override
    public void update(Observable o, Object temperature) {
        this.setTemperature((double) temperature);
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getTemperature() {
        return temperature;
    }

    public String getApplicationName() {
        return applicationName;
    }
    public void displayWeather() {
        System.out.println(this.getApplicationName() + " reports the current temperature is " + this.getTemperature() + " degrees celsius.");
    }

}

class OWeatherDemo {
    public static void main(String[] args) {
        OWeatherStation oWeatherStation = new OWeatherStation(); //observable
        OWeatherApplication app1 = new OWeatherApplication("Sinoptik"); //observer
        OWeatherApplication app2 = new OWeatherApplication("BNT"); //observer
        //add observers
        oWeatherStation.addObserver(app1);
        oWeatherStation.addObserver(app2);
        oWeatherStation.setWeather(23);

        app1.displayWeather();
        app2.displayWeather();

        // change the weather
        oWeatherStation.setWeather(30);

        app1.displayWeather();
        app2.displayWeather();
    }
}
