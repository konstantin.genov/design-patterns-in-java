package com.kgenov.DesignPatterns.Behavioral.Mediator;

import java.util.ArrayList;
import java.util.List;

/* konstantin created on 10/28/2020 inside the package - com.kgenov.DesignPatterns.Behavioral.Mediator */
/*
The Mediator design pattern facilitates the communication between components, by letting the components be unaware of each other's presence or absence in the system.
A mediator is used to reduce communication complexity between multiple objects or classes.
This pattern provides a mediator class which normally handles all the communications between different classes and supports easy maintenance of the code by loose coupling.

Motivation for using a mediator:
• Components may go in and out of a system at any time:
 - Chat room participants
 - Players in an MMORPG
• It makes no sense for participants to have direct references to one another
- Those references may go dead
• Solution: have then all refer to some central component that facilitates communication

The classic illustration of a mediator is the idea of a chat room - the reason for that is that the chatroom is precisely what the mediator is.
It is a way of letting users interact with one another without necessarily having direct references/pointers to one another. Instead, every message goes through
the chatroom, and the chatroom acts as a mediator.

That's precisely what I'm going to build down below.
 */
public class Mediator {
    //empty, used for filename
}

class Person {
    private String name;
    private ChatRoom room;
    private List<String> chatLog = new ArrayList<>();

    public Person(String name) {
        this.name = name;
    }

    public void receive(String msgSender, String message) {
        String s = msgSender + ": '" + message + "'";
        System.out.println("[" + name + "'s chat session] " + s);
        chatLog.add(s);
    }

    //broadcast msg to the chatroom
    public void publicMessage(String message) {
        room.broadcast(this.name, message);
    }

    public void privateMessage(String receiver, String message) {
        room.message(this.name, receiver, message);
    }

    public String getName() {
        return name;
    }

    public ChatRoom getRoom() {
        return room;
    }

    public void setRoom(ChatRoom room) {
        this.room = room;
    }

    public List<String> getChatLog() {
        return chatLog;
    }
}

class ChatRoom {
    private List<Person> people = new ArrayList<>();

    public void join(Person p) {
        String messageOnJoin = p.getName() + " joined the room!";
        broadcast("room", messageOnJoin);
        p.setRoom(this);
        people.add(p);
    }

    public void broadcast(String sender, String message) {
        for (Person person : people
        ) {
            // send msg to everyone except sender
            if (!person.getName().equals(sender)) {
                person.receive(sender, message);
            }
        }
    }

    public void message(String sender, String receiver, String message) {
        people.stream()
                .filter(p -> p.getName().equals(receiver))
                .findFirst()
                .ifPresent(p -> p.receive(sender, message));
    }
}

class MediatorDemo{
    public static void main(String[] args) {
        ChatRoom room = new ChatRoom();

        Person nikola = new Person("Nikola");
        Person mavrev = new Person("Mavrev");

        room.join(nikola); // no message here
        room.join(mavrev);

        nikola.publicMessage("hi everyone");
        mavrev.publicMessage("oh, hey dude");

        Person simon = new Person("Simon");
        room.join(simon);

        simon.publicMessage("hi everyone!");
        mavrev.privateMessage("Simon", "glad you could join us!");

    }
}
