package com.kgenov.DesignPatterns.Behavioral.Interpreter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/* konstantin created on 10/27/2020 inside the package - com.kgenov.DesignPatterns.Behavioral.Interpreter */
/*
Interpreter pattern provides a way to evaluate language grammar or expression. This type of pattern comes under behavioral pattern.
This pattern involves implementing an expression interface which tells to interpret a particular context.
This pattern is used in SQL parsing, symbol processing engine etc.

Now the thing about interpreter is this design pattern is actually a reflection of an entire field of computer science called compiler theory.
The main idea is that textual input needs to be processed:
- E.g., turned into OOP structures (when we're talking about Java)

Some examples:
• Programming language compilers, interpreters and IDEs. For example, IntelliJ performs static analysis, they have to interpret your code - has to interpret every
single construct that you write and give you hints and suggestions as to whether you are doing things right.
• HTML, XML and similar (interpreter reads it, understands it and changes is to something object-oriented.
• Numeric expressions (3+4/5) - in order for your computer to process this properly, it needs to be turned into some sort of OO structure before it can be traversed and evaluated
• Regular expressions - domain specific languages embedded into programming languages, which allows us to check certain strings and evaluate them

Let's recap, an interpreter is a component that processes structured text data. Does so by turning it into separate lexical tokens (lexing) and then interpreting
sequences of said tokens (parsing).
 */
public class Interpreter {
}

// simple lexing implementation; sample input: (13+4)-(12+1);
class Token {
    public enum Type {
        INTEGER,
        PLUS,
        MINUS,
        LPAREN, // //right parenthesis - (
        RPAREN //right parenthesis - )
    }

    public Type type;
    public String text;

    public Token(Type type, String text) {
        this.type = type;
        this.text = text;
    }

    @Override //surround each token with {}
    public String toString() {
        return "`" + text + "`";
    }
}

interface Element {
    int eval();
}

// used for parsing int token
class IntegerToken implements Element {
    private int value;

    public IntegerToken(int value) {
        this.value = value;
    }

    @Override
    public int eval() {
        return value;
    }
}

// handles parsing binary operations; addition, subtraction
class BinaryOperation implements Element {

    public enum Type {
        ADDITION,
        SUBTRACTION
    }

    public Type type;
    public Element left, right; // can be both binary op or integer -> no limit to the depth of this binary tree

    @Override
    public int eval() {
        switch (type) {
            case ADDITION:
                return left.eval() + right.eval();
            case SUBTRACTION:
                return left.eval() - right.eval();
            default:
                return 0;
        }
    }
}

class InterpreterDemo {
    //helper function to interpret string to tokens
    static List<Token> lex(String input) {
        ArrayList<Token> result = new ArrayList<>();

        for (int i = 0; i < input.length(); i++) {
            switch (input.charAt(i)) {
                case '+':
                    result.add(new Token(Token.Type.PLUS, "+"));
                    break;
                case '-':
                    result.add(new Token(Token.Type.MINUS, "-"));
                    break;
                case '(':
                    result.add(new Token(Token.Type.LPAREN, "("));
                    break;
                case ')':
                    result.add(new Token(Token.Type.RPAREN, ")"));
                    break;
                default: // check for integers
                    StringBuilder sb = new StringBuilder("" + input.charAt(i));
                    for (int j = i + 1; j < input.length(); j++) {
                        if (Character.isDigit(input.charAt(j))) {
                            sb.append(input.charAt(j));
                            i++; //increment outer loop so we don't go over the same element
                        } else {
                            // add all integer tokens from the sb
                            result.add(new Token(Token.Type.INTEGER, sb.toString()));
                            break;
                        }
                    }
                    break;
            }
        }
        return result;
    }

    static Element parse(List<Token> tokens) {
        BinaryOperation result = new BinaryOperation();
        boolean haveLHS = false; // indicator if we've gone over the left part of the binary operation

        for (int i = 0; i < tokens.size(); ++i) {
            Token token = tokens.get(i);
            switch (token.type) {
                case INTEGER:
                    IntegerToken integer = new IntegerToken(Integer.parseInt(token.text));
                    if (!haveLHS) { //havent processed left side yet
                        result.left = integer;
                        haveLHS = true;
                    } else { // proccess right if done with left
                        result.right = integer;
                    }
                    break;
                case PLUS:
                    result.type = BinaryOperation.Type.ADDITION;
                    break;
                case MINUS:
                    result.type = BinaryOperation.Type.SUBTRACTION;
                    break;
                case LPAREN: //recursively parse until reaching right parenthesis
                    int j = i; //location of right paren
                    for (; j < tokens.size() ; ++j)
                        if (tokens.get(j).type == Token.Type.RPAREN) break;

                        List<Token> subexpression = tokens.stream()
                                .skip(i+1)
                                .limit(j-i)
                                .collect(Collectors.toList());
                        Element element = parse(subexpression);
                        if (!haveLHS) {
                            result.left = element;
                            haveLHS = true;
                        } else {
                            result.right = element;
                        }
                        i = j;
                        break;
            }

        }
        return result;
    }

    public static void main(String[] args) {
        String input = "(13+4)-(12+1)";
        // perform lexing on our input
        List<Token> tokens = lex(input);
        System.out.println(tokens.stream()
                .map(Token::toString).
                        collect(Collectors.joining("\t")));

        System.out.println("==================");

        Element parsed = parse(tokens);
        System.out.println(input + " = " + parsed.eval());


    }
}