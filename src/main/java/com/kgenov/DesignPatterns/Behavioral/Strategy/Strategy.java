package com.kgenov.DesignPatterns.Behavioral.Strategy;
/* konstantin created on 10/31/2020 inside the package - com.kgenov.DesignPatterns.Behavioral.Strategy */

/*
Strategy Design Pattern is a type of behavioral design pattern that encapsulates a "family" of algorithms and selects one from the pool for use during runtime.
The algorithms are interchangeable, meaning that they are substitutable for each other.

The key idea is to create objects which represent various strategies. These objects form a pool of strategies from which the context object can choose from to vary its behavior as per its strategy.
These objects(strategies) perform the same operation, have the same(single) job and compose the same interface strategy.

Let’s take the sorting algorithms we have for example. Sorting algorithms have a set of rule specific to each other they follow to effectively sort an array of numbers.
Then, in our program, we need different sorting algorithms at a time during execution. Using SP allows us to group these algorithms and select from the pool when needed.
It is more like a plugin, like the plug & play in Windows or in the Device Drivers. All the plugins must follow a signature or rule.

Imagine we've created our own data structure, an array list, we've compiled our code and everything is running smoothly. However, at a certain time we run into a problem.
Our sorting algorithm is not optimal - it is slow, and has the wrong strategy for our data. Because we've tightly coupled our code, we'd need to stop running our program,
change the algorithm, compile again and hope for the best. This is a very bad approach - we might need the old algorithm at some point, or to introduce a new one.

So we have to design our code in a way we can provide our data structures with different sorting **strategies**. The SP heavily implies to use composition over inheritance, which will allow us to accomplish
the functionality which we need.

Solution:
- Implement a "strategy" interface, which will outline the functionality needed by each concrete strategy
- Delegate classes(strategies) while implementing the strategy interface -- All strategies must have the same signature
- Model our context class (the one using the strategies) to require a dependency injection for each one of it's algorithms (inject concrete strategies)

I've demonstrated the strategy pattern below.
 */
public class Strategy {
    //empty, used for filename
}

interface IFlyBehavior {
    void fly();
}

interface IQuackBehavior {
    void quack();
}

interface IWalkBehavior {
    void walk();
}
// Context class which takes strategies for different algorithms and can swap at runtime
class Duck {
    private String name;
    private IFlyBehavior flyBehavior;
    private IQuackBehavior quackBehavior;
    private IWalkBehavior walkBehavior;

    public Duck(String name, IFlyBehavior flyBehavior, IQuackBehavior quackBehavior, IWalkBehavior walkBehavior) {
        this.name = name;
        this.flyBehavior = flyBehavior;
        this.quackBehavior = quackBehavior;
        this.walkBehavior = walkBehavior;
    }

    public void fly(){
        this.flyBehavior.fly();
    }

    public void quack(){
        this.quackBehavior.quack();
    }

    public void walk(){
        this.walkBehavior.walk();
    }

    public void setFlyBehavior(IFlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void setQuackBehavior(IQuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }

    public void setWalkBehavior(IWalkBehavior walkBehavior) {
        this.walkBehavior = walkBehavior;
    }

    public String getName() {
        return name;
    }
}

// concrete implementations of behaviours

//flying
class SimpleFlying implements IFlyBehavior {

    @Override
    public void fly() {
        System.out.println("Simple flying!");
    }
}

class AdvancedFlying implements IFlyBehavior{

    @Override
    public void fly() {
        System.out.println("Flying in a very advanced way!");
    }
}
// quacking
class SimpleQuack implements IQuackBehavior {

    @Override
    public void quack() {
        System.out.println("Quacking in the most normal way. Quack quack.");
    }
}

class WeirdQuack implements IQuackBehavior {

    @Override
    public void quack() {
        System.out.println("Quacking in a very strange way. Quirk quirk.");
    }
}

// walking
class SimpleWalking implements IWalkBehavior {

    @Override
    public void walk() {
        System.out.println("Walking like a normal duck.");
    }
}

class FastWalking implements IWalkBehavior {

    @Override
    public void walk() {
        System.out.println("Walking very fast. You'd better run!");
    }
}

class StrategyDemo {
    public static void main(String[] args) {
        // create behaviors
        IFlyBehavior flyBehavior = new SimpleFlying();
        IQuackBehavior quackBehavior = new SimpleQuack();
        IWalkBehavior walkingBehavior = new SimpleWalking();

        Duck duck = new Duck("Bob", flyBehavior, quackBehavior, walkingBehavior);

        duck.fly();
        duck.quack();
        duck.walk();

        //change behaviour with a strategy at runtime
        System.out.println("\nChanging behaviour.. \n");
        duck.setFlyBehavior(new AdvancedFlying());
        duck.setQuackBehavior(new WeirdQuack());
        duck.fly();
        duck.quack();
        duck.walk();

    }
}