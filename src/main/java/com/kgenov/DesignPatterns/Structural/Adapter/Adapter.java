package com.kgenov.DesignPatterns.Structural.Adapter;

/*
The Adapter pattern allows otherwise incompatible classes to work together by converting the interface of one class
into an interface expected by the clients. Socket wrenches provide an example of the Adapter.

In the program below we are passing round pegs through round holes. At a certain point we introduce square pegs into our
program and we want to be able to pass them through the round holes. However, a square cannot pass through a circle,
this is where I will introduce the adapter that takes on a square object and re-shapes/re-models it into a circle, which can
then be passed through the hole;
 */
public class Adapter {
    //empty, used for filename only
}

interface RoundShape {
    double getRadius();
}

interface SquareShape {
    double getWidth();
}

// Client
class RoundPeg implements RoundShape {
    private double radius;

    public RoundPeg(double radius) {
        this.radius = radius;
    }

    @Override
    public double getRadius() {
        return radius;
    }

}

// Client
class SquarePeg implements SquareShape {
    private double width;

    public SquarePeg(double width) {
        this.width = width;
    }

    @Override
    public double getWidth() {
        return width;
    }
}

// Adaptee
class RoundHole implements RoundShape {
    private double radius;

    public RoundHole(double radius) {
        this.radius = radius;
    }

    @Override
    public double getRadius() {
        return radius;
    }

    public boolean fits(RoundShape peg) {
        boolean result;
        result = (this.getRadius() >= peg.getRadius());
        return result;
    }
}

// Adapter
class SquarePegAdapter implements RoundShape {
    private SquarePeg peg;

    // inject dependency
    public SquarePegAdapter(SquarePeg peg) {
        this.peg = peg;
    }

    @Override
    public double getRadius() {
        double result;
        // Calculate a minimum circle radius, which can fit this peg.
        result = (Math.sqrt(Math.pow((peg.getWidth() / 2), 2) * 2));
        return result;
    }
}

class AdapterDemo {
    public static void main(String[] args) {
        RoundPeg roundPeg = new RoundPeg(16);
        RoundHole roundHole = new RoundHole(20);
        SquarePeg squarePeg = new SquarePeg(10);
        roundHole.fits(roundPeg);
//        roundHole.fits(squarePeg); -- will not compile
        SquarePegAdapter squarePegAdapter = new SquarePegAdapter(squarePeg);
        roundHole.fits(squarePegAdapter);

    }
}