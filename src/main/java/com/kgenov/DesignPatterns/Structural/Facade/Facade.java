package com.kgenov.DesignPatterns.Structural.Facade;

import com.sun.prism.image.ViewPort;

import java.util.ArrayList;
import java.util.List;

/* konstantin created on 10/25/2020 inside the package - com.kgenov.DesignPatterns.Structural.Facade */
/*
The Facade provides a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use and
wraps a complicated subsystem with a simpler interface. It provides a simplified interface to a library, a framework, or any other complex set of classes.

For e.g.:
You have a complex video conversion framework, which has many framework classes providing all kinds of functionality.
Instead of making your code work directly with the above classes, you create a facade class which encapsulates that functionality and hides it from the rest of the code.
This structure also helps you to minimize the effort of upgrading to future versions of the framework or replacing it with another one. The only thing you'd need to change in
your app would be the implementation of the facade's methods.
 */
public class Facade {
}

class Buffer {
    private char[] characters;
    private int lineWidth;

    public Buffer(int lineHeight, int lineWidth) {
        this.lineWidth = lineWidth;
        characters = new char[lineWidth * lineHeight];
    }

    public char charAt(int x, int y) {
        return characters[y * lineWidth + x];
    }
}

class Viewport {
    private Buffer buffer;
    private final int width;
    private final int height;
    private final int offsetX;
    private final int offsetY;

    public Viewport(Buffer buffer, int width, int height, int offsetX, int offsetY) {
        this.buffer = buffer;
        this.width = width;
        this.height = height;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    // another implementation
    public char charAt(int x, int y) {
        return buffer.charAt(x + offsetX, y + offsetY);
    }
}
//pseudo code impl of a console system
class Console {
    private List<Viewport> viewports = new ArrayList<>();
    private int width, height;

    public Console(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void addViewPort(Viewport viewPort) {
        viewports.add(viewPort);
    }

    public void render() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                for (Viewport vp : viewports
                ) {
                    System.out.println(vp.charAt(x, y));
                }
                System.out.println();
            }
        }
    }

    // Facade implementation -- encapsulate the logic needed to create a new console
    public static Console newConsole(int width, int height){
        Buffer buffer = new Buffer(width, height);
        Viewport viewport = new Viewport(buffer, width, height, 0, 0);
        Console console = new Console(width, height);
        console.addViewPort(viewport);
        return console;

    }
}

// Now if the user wanted to create a console, he'd have to use all of our low level logic, instantiate a buffer, a view port, console, etc.
class Demo {
    public static void main(String[] args) {
        Buffer buffer = new Buffer(30, 20);
        Viewport viewport = new Viewport(buffer, 30, 20, 0, 0);
        Console console = new Console(30, 20);
        console.addViewPort(viewport);
        console.render();
        // The problem with this is that the user might not necessarily want to go through all of this low-level API
        // This is precisely where the Facade comes in. It will provide a simplified API over the set of sub-systems.

        // With Facade implemented
        Console console2 = Console.newConsole(20, 15);
        console2.render();

    }
}