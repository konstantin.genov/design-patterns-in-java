package com.kgenov.DesignPatterns.Structural.Proxy;
/* konstantin created on 10/26/2020 inside the package - com.kgenov.DesignPatterns.Structural.Proxy */

/*
Virtual Proxies wrap expensive objects and loads them on-demand. Sometimes we don't immediately need all functionalities that an object offers,
especially if it is memory/time-consuming. Calling objects only when needed might increase performance quite a bit.

One example of a virtual proxy is loading images. Let's imagine that we're building a file manager. Like any other file manager,
this one should be able to display images in a folder that a user decides to open. If we assume there exists a class, ImageViewer, responsible for loading and displaying images
- we might implement our file manager by using this class directly. This kind of approach seems logical and straight-forward but it contains a subtle problem.
If we implement the file manager as described above, we're going to be loading images every time they appear in the folder. If the user only wishes to see the name or size of an image,
this kind of approach would still load the entire image into memory. Since loading and displaying images are expensive operations, this can cause performance issues.

A better solution would be to display images only when actually needed. In this sense, we can use a proxy to wrap the existing ImageViewer object.
This way, the actual image viewer will only get called when the image needs to be rendered.
All other operations (such as obtaining the image name, size, date of creation, etc.) don't require the actual image and can therefore be obtained through a much lighter proxy object instead.
Code e.g. below!
 */

public class VirtualProxy {
    //empty, used for filename
}

interface ImageViewer {
    public void displayImage();
}
//pseudo image functionality
class Image {
    private String path;

    private Image(String path) {
        this.path = path;
    }
    public static Image load(String path){
        return new Image(path);
    }
    public void display(){
        System.out.println("Displaying image at " + this.path);
    }
}

// concrete impl.
class ConcreteImageViewer implements ImageViewer {

    private Image image;

    public ConcreteImageViewer(String path) {
        // Costly operation, takes long time to load an image. We'll wrap the constructor call which causes this loading in a proxy.
        // We'll make sure to only load an image (calling the constructor) when a client requests explicitly the displayImage() method
        this.image = Image.load(path);
    }

    @Override
    public void displayImage() {
        // Light-weight operation
        image.display();
    }
}

//Implement our lightweight image viewer proxy. This object will call the concrete image viewer only when needed, i.e. when the client calls the displayImage() method.
//Until then, no images will be loaded or processed, which will make our program much more efficient.
class ImageViewerProxy implements ImageViewer {

    private String path;
    private ImageViewer viewer;

    public ImageViewerProxy(String path) {
        this.path = path;
    }

    @Override
    public void displayImage() {
        this.viewer = new ConcreteImageViewer(this.path);
        this.viewer.displayImage();
    }
}

// Client-side code simulation
class VirtualProxyDemo{
    // In the code below, we are creating six different image viewers. First, three of them are the concrete image viewers that automatically load images on creation.
    // The last three images don't load any images into memory on creation.
    public static void main(String[] args) {
        // all 3 concrete viewers load the images the moment they are created, thus leading to a very expensive operation
        ImageViewer flowers = new ConcreteImageViewer("./photos/flowers.png");
        ImageViewer trees = new ConcreteImageViewer("./photos/trees.png");
        ImageViewer grass = new ConcreteImageViewer("./photos/grass.png");

        // only with the last line will the first proxy viewer start loading the image. Compared to the concrete viewers, performance benefits are obvious:
        ImageViewer sky = new ImageViewerProxy("./photos/sky.png");
        ImageViewer sun = new ImageViewerProxy("./photos/sun.png");
        ImageViewer clouds = new ImageViewerProxy("./photos/clouds.png");

        sky.displayImage();
    }
}