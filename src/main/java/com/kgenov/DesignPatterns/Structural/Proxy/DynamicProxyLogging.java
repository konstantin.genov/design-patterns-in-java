package com.kgenov.DesignPatterns.Structural.Proxy;
/* konstantin created on 10/26/2020 inside the package - com.kgenov.DesignPatterns.Structural.Proxy */
/*
Dynamic proxies differ from static proxies in a way that they do not exist at compile time.
Instead, they are generated at runtime by the JDK and then made available to the users at runtime.
We need to understand the following two components to write a dynamic proxy.
- When writing a dynamic proxy, the principal task of the programmer is to write an object called an invocation handler​, which implements the InvocationHandler interface
from the java.lang.reflect ​package.
An invocation handler intercepts call to the implementation, performs some programming logic, and then passes on the request to the implementation.
========
In the e.g. below, I will implement a simple LoggingHandler which will count the number of times a method from the Person class is invoked(this is our programming logic).
 */

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class DynamicProxyLogging {
    //empty, used for filename
}

class LoggingHandler implements InvocationHandler {
    private final Object target;
    private Map<String, Integer> calls = new HashMap<>();

    LoggingHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String name = method.getName();

        if (name.contains("toString")) {
            return calls.toString();
        }

        // add or increment number of calls
        calls.merge(name, 1, Integer::sum);
        return method.invoke(target, args);
    }
}

interface Human {
    void walk();

    void talk();
}

class Person implements Human {
    @Override
    public void walk() {
        System.out.println("I am walking");
    }

    @Override
    public void talk() {
        System.out.println("I am talking");
    }
}

class DynamicLoggingProxyDemo {
    @SuppressWarnings("unchecked")
    public static <T> T withLogging(T target, Class<T> itf) {
        return (T) Proxy.newProxyInstance(
                itf.getClassLoader(),
                new Class<?>[]{itf},
                new LoggingHandler(target));
    }

    public static void main(String[] args) {
        Person person = new Person();
        Human logged = withLogging(person, Human.class);
        logged.walk();
        logged.talk();
        logged.talk();

        System.out.println(logged);
    }
}
