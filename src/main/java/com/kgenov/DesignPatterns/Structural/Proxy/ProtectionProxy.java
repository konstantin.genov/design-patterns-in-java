package com.kgenov.DesignPatterns.Structural.Proxy;
/* konstantin created on 10/26/2020 inside the package - com.kgenov.DesignPatterns.Structural.Proxy */

/*
Protection Proxies are used for checking certain conditions. Some objects or resources might need appropriate authorization for accessing them, so using a proxy is one of the ways
in which such conditions can be checked.
With protection proxies, we also get the flexibility of having many variations of access control.

For example, we have a car, and a car should only be driven by people who are at least 16 years old or older.
Our Car class will have a method drive(), which should only be used by drivers of the appropriate age. In order to accomplish this,
we'll create a CarProxy class which will make a call to the super.() if the conditions are met
 */

public class ProtectionProxy {
}

interface Driveable{
    void drive();
}

class Driver {
    private int age;

    public Driver(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }
}

class Car implements Driveable {
    //aggregate
    protected Driver driver;

    public Car(Driver driver) {
        this.driver = driver;
    }

    @Override
    public void drive() {
        System.out.println("Car is being driven.");
    }
}

// Protected proxy which provides authorization for access
class CarProxy extends Car {
    public CarProxy(Driver driver) {
        super(driver);
    }
    @Override
    public void drive() {
        if (super.driver.getAge() >= 16) {
            super.drive();
        } else  {
            System.out.println("Driver is too young to drive the car.");
        }
    }

}

class ProtectedProxyDemo {
    public static void main(String[] args) {
        Car car = new CarProxy(new Driver(15));
        car.drive();
    }
}