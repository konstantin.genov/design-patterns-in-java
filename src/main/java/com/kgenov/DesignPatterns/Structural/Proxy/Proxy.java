package com.kgenov.DesignPatterns.Structural.Proxy;

/* konstantin created on 10/26/2020 inside the package - com.kgenov.DesignPatterns.Structural.Proxy */
/*
The proxy pattern means using a proxy for some other entity. In other words, a proxy is used as an intermediary in front of, or wrapped around, an existing object.
This can be used, for example, when the actual object is very resource-intensive or when there are certain conditions which need to be checked before using the actual object.
A proxy can also be useful if we'd like to limit the access or functionality of an object.

Intent:
- Provide a surrogate or placeholder for another object to control access to it.
- Use an extra level of indirection to support distributed, controlled, or intelligent access.
- Add a wrapper and delegation to protect the real component from undue complexity.


 */
public class Proxy {
    //empty, used for filename
}

// I have split the examples in different outer classes(files) for each type of proxy: Protection Proxy, Property Proxy and Virtual Proxy