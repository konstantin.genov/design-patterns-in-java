package com.kgenov.DesignPatterns.Structural.Decorator;
/* konstantin created on 10/24/2020 inside the package - com.kgenov.DesignPatterns.Structural.Decorator */

/*
In this example, we've created the Shape interface, Circle and Square classes. Let's pretend we can't go back in our code and change it, we've built a giant application
and changing this code is impossible. We do not want to violate the open-closed principle, so instead we're gonna create a dynamic decorator.
Firstly, we're gonna add a ColoredShape to extend our functionality, then we'll finish of by adding a TransparentShape which gives us even more functionality.
 */
public class DynamicDecorator {
    //empty, used for filename
}

interface Shape {
    String info();
}

class Square implements Shape {
    private float side;

    public Square(float side) {
        this.side = side;
    }

    @Override
    public String info() {
        return "This Square has a side of " + side;
    }

    @Override
    public String toString() {
        return "Square{" +
                "side=" + side +
                '}';
    }
}

class Circle implements Shape {
    private float radius;

    public Circle(float radius) {
        this.radius = radius;
    }

    @Override
    public String info() {
        return "This Circle has a radius of " + radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }
}

// Now we introduce our first decorator class by locally composing a shape object and adding an additional field
class ColoredShape implements Shape {
    // Delegate shape
    private Shape shape;
    //add additional field to extend functionality
    private String color;

    public ColoredShape(Shape shape, String color) {
        this.shape = shape;
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String info() {
        return shape.info() + " has the color " + color;
    }

    @Override
    public String toString() {
        return "ColoredShape{" +
                "shape=" + shape +
                ", color='" + color + '\'' +
                '}';
    }
}

// Second decorator which gives us the ability to create transparent shapes
class TransparentShape implements Shape {
    private Shape shape;
    private int transparency;

    public TransparentShape(Shape shape, int transparency) {
        this.shape = shape;
        this.transparency = transparency;
    }

    @Override
    public String info() {
        return shape.info() + " has " + transparency + "% transparency";
    }

    @Override
    public String toString() {
        return "TransparentShape{" +
                "shape=" + shape +
                ", transparency=" + transparency +
                '}';
    }
}

class DemoDynamicDecorator {
    public static void main(String[] args) {
        Circle normalCircle = new Circle(10);
        // now we can decorate our circle by adding color to it
        System.out.println(normalCircle);
        ColoredShape coloredCircle = new ColoredShape(normalCircle, "red");
        System.out.println(coloredCircle);
        // finally decorate our object by adding transparency
        TransparentShape transparentColoredShape = new TransparentShape(coloredCircle, 40);
        System.out.println(transparentColoredShape);
        System.out.println(transparentColoredShape.info());
    }
}