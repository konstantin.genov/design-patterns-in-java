package com.kgenov.DesignPatterns.Structural.Composite;

import sun.security.util.ArrayUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Composite is a structural design pattern that lets you compose objects into tree structures and then work with
these structures as if they were individual objects.

Using the Composite pattern makes sense only when the core model of your app can be represented as a tree.

For example, imagine that you have two types of objects: Products and Boxes.
A Box can contain several Products as well as a number of smaller Boxes.
These little Boxes can also hold some Products or even smaller Boxes, and so on.

Say you decide to create an ordering system that uses these classes.
Orders could contain simple products without any wrapping, as well as boxes stuffed with products...and other boxes.
How would you determine the total price of such an order?

You could try the direct approach: unwrap all the boxes, go over all the products and then calculate the total.
That would be doable in the real world; but in a program, it’s not as simple as running a loop. You have to know the classes of Products and Boxes you’re going through, the nesting level of the boxes and other nasty details beforehand.
All of this makes the direct approach either too awkward or even impossible.

The Composite pattern suggests that you work with Products and Boxes through a common interface which declares a method for calculating the total price.
The greatest benefit of this approach is that you don’t need to care about the concrete classes of objects that compose the tree.
You don’t need to know whether an object is a simple product or a sophisticated box.
You can treat them all the same via the common interface.
When you call a method, the objects themselves pass the request down the tree.
 */
public class Composite {
    // empty, used for filename
}

// Component - common interface between the composite and the leaf
interface TodoList {
    String getHtml();
}

// leaf
class Todo implements TodoList {

    public String text;

    public Todo(String text) {
        this.text = text;
    }

    @Override
    public String getHtml() {
        return this.text;
    }
}

// composite of components
class Project implements TodoList {

    private String title;
    private List<TodoList> components = new ArrayList<>();

    public Project(String title, List<TodoList> components) {
        this.title = title;
        this.components = components;
    }

    @Override
    // very crude example; need something immutable tbh
    public String getHtml() {
        String html = "<h1>";
        html += this.title;
        html += "</h1><ul>";
        for (TodoList tl : components
        ) {
            html += "<li>";
            html += tl.getHtml();
            html += "</li>";
        }
        html += "/ul";
        return html;
    }
}

class Demo {
    public static void main(String[] args) {
        List<TodoList> todos = new ArrayList<>(Arrays.asList(
                new Todo("Hello"),
                new Todo("My name is Slim"),
                new Project("My Project", new ArrayList<>(Arrays.asList(
                        new Todo("In the project"), new Todo("Again in the project"))))));

        todos.forEach(todo -> System.out.println(todo.getHtml()));
    }
}