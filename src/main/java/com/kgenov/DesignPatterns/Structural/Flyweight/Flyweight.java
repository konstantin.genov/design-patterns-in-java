package com.kgenov.DesignPatterns.Structural.Flyweight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/* konstantin created on 10/25/2020 inside the package - com.kgenov.DesignPatterns.Structural.Flyweight */
/*
Flyweight pattern is primarily used to reduce the number of objects created and to decrease memory footprint and increase performance.
Flyweight pattern tries to reuse already existing similar kind objects by storing them and creates new object when no matching object is found.
=====
E.g:
Imagine you made a simple game - players would be moving around a map and shooting each other.
You chose to implement a realistic particle system and make it a distinctive feature of the game. Vast quantities of bullets, missiles, and shrapnel
from explosions should fly all over the map and deliver a thrilling experience to the player.

You've completed the game and it's time to play! You send it to a few friends so they can have some fun.
Although the game was running flawlessly on your machine, your some of your friends weren't able to play for so long.
On their computers, the game kept crashing after a few minutes of gameplay. After spending several hours digging through debug logs,
you discovered that the game crashed because of an insufficient amount of RAM.
It turned out that some of your friends rigs were much less powerful than your own computer, and that’s why the problem emerged so quickly on their machines

The actual problem was related to your particle system. Each particle, such as a bullet, a missile or a piece of shrapnel was represented by a separate object containing plenty of data.
At some point, when the carnage on a player’s screen reached its climax, newly created particles no longer fit into the remaining RAM, so the program crashed.
This issue can be solved by introducing the Flyweight pattern.
 .*/
public class Flyweight {
    //empty, used for filename
}
// E.g of flyweight with repeating user names below.


//without flyweight
class User {
    private String fullName;

    public User(String fullName) {
        this.fullName = fullName;
    }
}
// with flyweight
class UserFlyweight {
    static List<String> strings = new ArrayList<>();
    private int[] names;

    public UserFlyweight(String fullName) {
        // checks if string exists in the list, if it does - returns it's index; if it doesn't - add it to the list and return it's index
        // adds the returned index in the int names arr; the values correspond 1:1 with the indexes of the strings in the list
        // E.g: John Smith -> John[0], Smith[1]; Then add Jane Smith -> Jane[2], Smith[1] (as it exists in the list already)
        Function<String, Integer> getOrAdd = (String s) -> {
            int idx = strings.indexOf(s);
            if (idx != -1) return idx;
            else {
                strings.add(s);
                return strings.size() - 1;
            }
        };

        names = Arrays.stream(fullName.split(" "))
                .mapToInt(getOrAdd::apply).toArray();
    }

    public String getFullName() {
        return Arrays.stream(names).mapToObj(i -> strings.get(i))
                .collect(Collectors.joining(","));
    }

    public static List<String> getStrings() {
        return strings;
    }

    public int[] getNames() {
        return names;
    }
}

class UsersDemo {
    public static void main(String[] args) {
        // with this approach we save 5 bytes as John and Jane share the last name
        // have "Smith" in common
        UserFlyweight user = new UserFlyweight("John Smith");
        UserFlyweight user1 = new UserFlyweight("Jane Smith");
        System.out.println(user1.getFullName());


    }
}