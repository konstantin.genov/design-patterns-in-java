package com.kgenov.DesignPatterns.Structural.Bridge;

/*
The bridge design pattern is a structural pattern used to decouple an abstraction from its implementation so that the two can vary independently.

Bridge patterns decouple the abstract class and its implementation classes by providing a bridge structure between them.
This bridge uses encapsulation, aggregation, and inheritance to separate responsibilities into various classes.

The bridge pattern is useful when both the class and what it does vary, often because changes in the class can be made easily
with minimal prior knowledge about the program.

We can think of the class itself as an abstraction, what the class can do as the implementation, and the bridge pattern, itself, as two layers of abstraction.

The bridge pattern allows us to avoid compile-time binding between an abstraction and its implementation. This is so that an implementation can be selected
at run-time.In other words, by using the bridge pattern, we can configure an abstraction with an implementor object at run-time.

You should only use the Bridge pattern if there is actual underlying structure that allows you to SPLIT the behaviour between the abstraction and implementation.
 */
public class Bridge {
    //empty. used for file name
}


// Abstraction
abstract class View {
    private IResource resource;

    public View(IResource resource) {
        this.resource = resource;
    }


    public View() {
    }

    public IResource getResource() {
        return resource;
    }

    public abstract String show();
}

// Implementor
interface IResource {
    String snippet();

    String title();

    String url();
}

// Refined abstraction
class LongFormView extends View {
    public LongFormView(IResource resource) {
        super(resource);
    }

    @Override
    public String show() {
        return this.getResource().snippet();
    }
}

// Concrete implementor
class ArtistResource implements IResource {
    private Artist artist;

    public ArtistResource(Artist artist) {
        this.artist = artist;
    }

    @Override
    public String snippet() {
        return this.artist.bio();
    }

    @Override
    public String title() {
        return artist.firstName + " " + artist.lastName + "'s Book of Wonders";
    }

    @Override
    public String url() {
        return "shopify.store.com/book-of-wonders.html";
    }
}

class Artist {
    public String firstName, lastName;

    public Artist(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String bio() {
        return "This guy lived";
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}