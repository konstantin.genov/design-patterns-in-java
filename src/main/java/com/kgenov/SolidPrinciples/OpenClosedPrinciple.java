package com.kgenov.SolidPrinciples;
/* konstantin created on 7/26/2020 inside the package - com.kgenov.SOLID_Principles */

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class OpenClosedPrinciple {
    /*
    In this demo I will demonstrate a description of the Open-Close Principle (OCP) with the implementation of a design pattern.
    This design pattern is not a GoF pattern, it's a pattern of enterprise engineering - it is called Specification.
    We will look at OCP through the prism of implementing the Specification design pattern.
    The whole point of OCP is to be open for extension, but closed for modification.
     */
    public static void main(String[] args) {
        // Demo with bad approach:
        Product apple = new Product("Apple", Color.RED, Size.SMALL);
        Product tree = new Product("Tree", Color.RED, Size.LARGE);
        Product house = new Product("House", Color.BLUE, Size.LARGE);

        List<Product> products = Arrays.asList(apple, tree, house);

        ProductFilter productFilter = new ProductFilter();
        System.out.println("Red products (old): ");
        productFilter.filterByColor(products, Color.RED).forEach(p -> System.out.println(" - " + p.name + " is red"));

        // Demo with OCP in mind
        BetterFilter betterFilter = new BetterFilter();
        System.out.println("Red products (new): ");
        betterFilter.filter(products, new ColorSpecification(Color.RED))
                .forEach(p -> System.out.println(" - " + p.name + " is red. [OCP]"));

        System.out.println("Large Red Products: ");
        betterFilter
                .filter(products, new AndSpecification<>(new SizeSpecification(Size.LARGE), new ColorSpecification(Color.RED)))
                .forEach(p -> System.out.println(" - Large " + p.color + " " + p.name));
    }
}


//Lets suppose you decide to make a website where you're selling different products (Amazon) and you want to allow your
//users to filter products by a specific criteria. Let's suppose our products have different colors:
enum Color {
    RED, GREEN, BLUE
}

enum Size {
    SMALL, MEDIUM, LARGE, HUGE
}

class Product {
    public String name;
    public Color color;
    public Size size;

    public Product(String name, Color color, Size size) {
        this.name = name;
        this.color = color;
        this.size = size;
    }

}
// I'm going to define a mechanism which will allow me to take a list of products and filter them by a particular criteria

class ProductFilter {

    public Stream<Product> filterByColor(List<Product> products, Color color) {
        return products.stream().filter(p -> p.color == color);
    }

    // Let's suppose we write this code and we push it to production, but then the manager/our boss
    // comes back and says we also want to filter products by size. So obviously you start doing some sort of cut/copy paste programming
    public Stream<Product> filterBySize(List<Product> products, Size size) {
        return products.stream().filter(p -> p.size == size);
    }
    /*
    Once again you've had to jump into a chunk of code you've already written and tested, and you have to modify it.
    !! REMEMBER: The whole point of the Open-Closed Principle (OCP) is to be open for extension but closed for modification and we are performing such here.
    The takeaway here is that modifications of code that has already been written and tested is not great.
     */

    // The boss comes back and wants to allow users to filter by both color and size. Once again, we jump into the ProductFilter and implement another method.
    public Stream<Product> filterBySizeAndColor(
            List<Product> products,
            Color color,
            Size size
    ) {
        return products.stream().filter(p -> p.size == size && p.color == color);
    }

    /*
    What is the problem of this code you may ask?
    The problem is that this is okay for let's say 2 criteria, we have sizes and products.
    Imagine if you had 3 criteria, for example: in addition to the above you also had price.
    So, now you would end up with 7 different methods if you want to have every intersection. That is problematic.
    That is something you don't want to do.
    You want code which is open for extension, but closed for modification. You want some sort of filtering mechanism which
    is closed to modification after it's been tested and shipped to your clients, as they might already be using a binary snapshot of this code.
    You don't want to keep modifying it. I'll demo the current approach in the main method.
     */
}

// Specification Design Pattern implementation below
interface Specification<T> {
    boolean isSatisfied(T item);
}

// here we setup the generic filter method needed to be implemented by our new filter
interface Filter<T> {
    Stream<T> filter(List<T> items, Specification<T> specification);
}

// this is a color speficiation which implements the specification interface and performs the boolean predicate check which can later be used
// in our new filter class by directly calling the isSatisfied() on the object
class ColorSpecification implements Specification<Product> {
    private Color color;

    public ColorSpecification(Color color) {
        this.color = color;
    }

    @Override
    public boolean isSatisfied(Product p) {
        return p.color == color;
    }
}

class SizeSpecification implements Specification<Product> {
    private Size size;

    public SizeSpecification(Size size) {
        this.size = size;
    }

    @Override
    public boolean isSatisfied(Product p) {
        return p.size == size;
    }
}

// When we need to filter items by more than 1 specification/s, in this example 2, we need to create seperate new classes which implement
// the Specification interface and extend the verification functionality
class AndSpecification<T> implements Specification<T>{
    private Specification<T> first, second;

    public AndSpecification(Specification<T> first, Specification<T> second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public boolean isSatisfied(T item) {
        return first.isSatisfied(item) && second.isSatisfied(item);
    }
}

class BetterFilter implements Filter<Product>{
    @Override
    public Stream<Product> filter(List<Product> items, Specification<Product> specification) {
        return items.stream().filter(p -> specification.isSatisfied(p));
    }
}

