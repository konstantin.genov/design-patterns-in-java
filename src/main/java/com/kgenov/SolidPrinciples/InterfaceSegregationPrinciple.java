package com.kgenov.SolidPrinciples;
/* konstantin created on 7/29/2020 inside the package - com.kgenov.SOLID_Principles */

public class InterfaceSegregationPrinciple {

    /*
    The Interface Segregation Principle (ISP) states that a client should not be exposed to methods it doesn’t need.
    Declaring methods in an interface that the client doesn’t need pollutes the interface and leads to a “bulky” or “fat” interface.

    This means that you shouldn't overpopulate an interface with methods it might not need. This is where the YAGNI idea comes in.
    YAGNI - Ya Ain't gonna need it - this is a good thing to remember. In the below code, I will show an example of having an interface
    called Machine which has 3 methods, and how having all of these methods in one interface can lead to bad design decisions and why it should be avoided.
    Take a look at the Document class, Machine interface, etc;
     */
    class Document {

    }

    // This is an example of an overpopulated interface, below you will see an example of how this can be problematic
    interface Machine {
        void print(Document d);

        void fax(Document d);

        void scan(Document d);

    }

    // This class is perfectly fine, as we are presen ting an abstraction of a modern printer which has all of the functionality of the Machine interface.
    class MultiFunctionalPrinter implements Machine {
        @Override
        public void print(Document d) {

        }

        @Override
        public void fax(Document d) {

        }

        @Override
        public void scan(Document d) {

        }
    }

    // Now, here's where things get complicated. Imagine your client wants to create an old fashioned printer, which can only print documents.
    // Here is where we run into a problem - this printer should only have the ability to print, but by extending the interface we receive
    // more functionality than our printer can handle. So what do we do? Take a look at the comments in the fax/scan methods.
    class OldFashionedPrinter implements Machine {
        @Override
        public void print(Document d) {
            // implement logic for printing
        }

        @Override
        public void fax(Document d) {
            // Now what do we do here? Our old printer cannot fax documents, but we need to implement the method in some way.
            // One way is to throw an exception, such as a custom exception method not implemented. Below I'll just use the exception base class.
//            throw new Exception(); // But now we have a problem with our design hierarchy.
            // In order for us to be able to throw an exception, we need to add this signature to the Machine interface.
            // Imagine a situation where we do not have access to the source code of the interface, we receive it from a third party and we cannot make those changes.
            // Neither should we, we do not want the Machine interface to throw an exception in a perfectly valid class such as the MultiFunctionalPrinter
        }

        @Override
        public void scan(Document d) {
            // Refer to the comments above in the fax method.
        }
    }


    // Below, I will demonstrate the Interface Segregation principle and how it solves our current issue.

    // The approach I am taking will segregate the Machine interface into 3 different ones, which allows us to be flexible and avoid such problems.

    interface Printer {
        void print(Document d);
    }
    interface Scanner {
        void scan(Document d);
    }
    interface Fax {
        void fax(Document d);
    }

    // Now, if our client just wants an ordinary printer, he can easily do so by extending the specific interface with said functionality.

    class JustAPrinter implements Printer{
        @Override
        public void print(Document d) {

        }
    }

    // If he wants more functionality, he can implement as many as interfaces that he needs, or create a joining interface as demonstrated below

    class AdvancedPrinter implements Printer, Fax, Scanner{
        @Override
        public void print(Document d) {

        }

        @Override
        public void scan(Document d) {

        }

        @Override
        public void fax(Document d) {

        }
    }

    // Now with a joining interface

    interface MultiFunctionalDevice extends Printer, Fax, Scanner{
        // no need to implement any methods as they are inherited
    }

    class AnotherAdvancedPrinter implements MultiFunctionalDevice{
        @Override
        public void print(Document d) {

        }

        @Override
        public void scan(Document d) {

        }

        @Override
        public void fax(Document d) {

        }
    }

    /*
    Interface segregation is important, remember YAGNI - Ya Aint Gonna Need It when you are designing interfaces for your application. : )
     */
}
