package com.kgenov.SolidPrinciples;
/* konstantin created on 7/26/2020 inside the package - com.kgenov.SOLID_Principles */


public class LiskovSubstitutionPrinciple {
    /*
    A great example illustrating LSP is how sometimes something that sounds right in natural language doesn't quite work in code.
    In mathematics, a Square is a Rectangle. Indeed it is a specialization of a rectangle.
    The "is a" makes you want to model this with inheritance.
    However if in code you made Square derive from Rectangle, then a Square should be usable anywhere you expect a Rectangle.
    This makes for some strange behavior.

    The idea of the Liskov Substitution Principle is you should be able to substitute a subclass for a base class without breaking anything.
    Below, I will demonstrate this violation by using Rectangle as a base class and Square as a subclass.
     */

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(2, 3);
        useIt(rectangle);

        // Below example violates LSP
        Rectangle square = new Square();
        square.setWidth(5);
        useIt(square); // when the call to getArea() is made, we will get a wrong calculation, as a square's area is calculated as A = a^2
        // This happens because we have forgotten to override the getArea method in the Square subclass, thus violating the Liskov Principle
    }

    // this method will invoke Rectangle.getArea, and as it is not overridden in the subclass, this will lead to a strange behaviour
    public static void useIt(Rectangle r) {
        int width = r.getWidth();
        r.setHeight(10);
        System.out.println("Expected area of " + (width * 10) + ", got " + r.getArea());
    }
}

class Rectangle {
    protected int width, height;

    public Rectangle() {
    }

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getArea() {
        return width * height;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    public boolean isSquare() {
        return width == height;
    }
}

class Square extends Rectangle {
    public Square() {
    }

    public Square(int size) {
        width = height = size;
    }

    @Override
    public void setWidth(int width) {
        super.setWidth(width);
        super.setHeight(width);
    }

    @Override
    public void setHeight(int height) {
        super.setHeight(height);
        super.setWidth(height);
    }

}

