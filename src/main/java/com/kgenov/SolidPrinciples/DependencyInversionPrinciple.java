package com.kgenov.SolidPrinciples;

import org.javatuples.Triplet;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
/* konstantin created on 7/31/2020 inside the package - com.kgenov.SOLID_Principles */

public class DependencyInversionPrinciple {
    /*
    The dependency inversion principle is split into two parts:

    A. High-level modules should not depend on low-level modules. Both should depend on abstractions.

    First of all, what is an abstraction? Typically by abstraction we mean an abstract class or an interface.
    By abstraction, we mean that you get a signature of something that performs a particular operation, but you
    don't necessarily work with the concrete implementation, instead you're working with an abstraction - that's what an abstract class
    or an interface actually is.

    B. Abstractions should not depend on details. Details should depend on abstractions.

    If you can use abstract classes or interfaces instead of instead of concrete classes, do so.
    Because, obviously the benefit here is you can substitute one implementation for another without breaking your code.
     */


    // Below is a demo of part A
    // Let's suppose we want to have some sort of application for modeling relationships between different people - kind of like a family tree.

    public static void main(String[] args) {
        // Go through the code to see the structure and code used for the first demo
        Demo.run();

    }
}

enum Relationship {
    PARENT,
    CHILD,
    SIBLING
}

class Person {
    public String name;

    public Person(String name) {
        this.name = name;
    }
}

// now we need something to help us model the different relationships between people

class Relationships { // Low-level module - it's low level because it's related to data storage, simply provides a list,
    // and provides us access. Does not provide any business logic.
    private List<Triplet<Person, Relationship, Person>> relationships = new ArrayList<>();

    public List<Triplet<Person, Relationship, Person>> getRelationships() {
        return relationships;
    }

    public void addParentAndChild(Person parent, Person child) {

        // the way we've set this up, we'll have to add the parent and child twice as we have the relationship both-ways
        relationships.add(new Triplet<>(parent, Relationship.PARENT, child));
        relationships.add(new Triplet<>(child, Relationship.CHILD, parent));

    }
}

// something to help us research the relationships between people

class Research { // High-level module - it allows us to perform some operations on those low level constructs.
    // Remember that DIP states higher level modules should not depend on lower level ones.

//    public Research(Relationships relationships) {
//        List<Triplet<Person, Relationship, Person>> relations = relationships.getRelationships();
//
//        // Perform our research, let's say we want to find all of John's children
//        relations.stream()
//                .filter(x -> x.getValue0().name.equals("John") && x.getValue1() == Relationship.PARENT)
//                .forEach(ch -> System.out.println(
//                        "John has a child called " + ch.getValue2().name
//                ));
//    }

    public Research(RelationshipBrowser browser, String name){
        List<Person> children = browser.findAllChildrenOf(name);

        for (Person child : children){
            System.out.println(name + " has a child called " + child.name);
        }
    }
}

// The demo is executed in the main method of the DependencyInversionPrinciple class

class Demo {
    public static void run() {
        Person parent = new Person("John");
        Person child1 = new Person("Einstein");
        Person child2 = new Person("Obama");

        Relationships relationships = new Relationships();
        relationships.addParentAndChild(parent, child1);
        relationships.addParentAndChild(parent, child2);

//        new Research(relationships, "John");

        // Now with DIP approach

        RelationshipsDIP relationshipsDIP = new RelationshipsDIP();
        relationshipsDIP.addParentAndChild(parent, child1);
        relationshipsDIP.addParentAndChild(parent, child2);
        new Research(relationshipsDIP, parent.name);
    }
}

/*
    Everything seems to be working and you are probably wondering what's wrong with this code.
    Well, in actual fact there is a fairly major problem - we are exposing internal storage implementation of "relationships" as a public getter
    for everyone to access. This is a good time to discuss high-level and low-level modules (I will put a comment next to each module for its level)
    (Currently, high-level modules are depending on low-level ones)
    You are probably wondering - how can we actually fix it? Remember the second hint of DIP was that you should depend on abstractions instead.
    So now, I'm going to introduce an abstraction with an interface below.
 */


// This is the abstraction you are expected to implement on the low level module (class Relationships)
interface RelationshipBrowser {
    List<Person> findAllChildrenOf(String name);
}


// This way the search doesn't happen in the high-level module (Research), the search is happening in the core functionality of the low level module
// What's the reason for this? Let's suppose you want to change the implementation from a List to something else. With a direct dependancy on a low level module
// you cannot do this because we are getting relationships as a list, if you change it, you are in trouble, as you'll have to re-write lots of code
// Instead we are doing everything in the low-level module
class RelationshipsDIP implements RelationshipBrowser {
    private List<Triplet<Person, Relationship, Person>> relationships = new ArrayList<>();

    public List<Triplet<Person, Relationship, Person>> getRelationships() {
        return relationships;
    }

    public void addParentAndChild(Person parent, Person child) {

        relationships.add(new Triplet<>(parent, Relationship.PARENT, child));
        relationships.add(new Triplet<>(child, Relationship.CHILD, parent));

    }

    @Override
    public List<Person> findAllChildrenOf(String name) {
        return relationships.stream()
                .filter(x -> Objects.equals(x.getValue0().name, name)
                && x.getValue1() == Relationship.PARENT)
                .map(Triplet::getValue2)
                .collect(Collectors.toList());
    }
}