package com.kgenov.SolidPrinciples;

/* konstantin created on 7/26/2020 inside the package - com.kgenov */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SingleResponsibilityPrinciple {
    /*
    The Single Responsibility Principle (SRP) states that a class should have just a single reason to change.
    In other words, a single class should have one primary responsibility instead of taking on lots and lots
    of different responsibilities. If you take lots of responsibilities you end up with something called
    a god object, which is an anti-pattern.
     */
    public static void main(String[] args) throws IOException {
        // Demo Journal
        Journal journal = new Journal();
        journal.addEntry("I felt good today");
        journal.addEntry("I ate a hamburger");
        System.out.println(journal);

        // Demo with Persistence class approach
        Persistence persistence = new Persistence();
        String fileName = "c:\\temp\\journal.txt";
        persistence.saveToFile(journal, fileName, true);

        Runtime.getRuntime().exec("notepad.exe " + fileName);
    }
}

class Journal {
    private final List<String> entries = new ArrayList<>();
    private static int count = 0;

    /*
       All of the functionality below is directly relevant to the journal and we are sticking to SRP.
       The journal has a primary responsibility of storing entries, and armed with this we can now start using the journal.
    */

    public void addEntry(String text) {
        entries.add("" + (++count) + ": " + text);
    }

    public void removeEntry(int index) {
        entries.remove(index);
    }

    @Override
    public String toString() {
        return String.join(System.lineSeparator(), entries);
    }

    // ----- BREAKING THE SINGLE RESPONSIBILITY PRINCIPLE -----
    // I'm going to break the SRP by adding to the journal some of the stuff that doesn't necessarily belong to the journal

    //It might seem like a good idea to persist the journal in a file. One approach would be to add a save method.
    public void save(String fileName) throws FileNotFoundException {
        try (PrintStream out = new PrintStream(fileName)) {
            out.println(toString());
        }
    }

    /* Later on, you'd might want to load the journal from a file, so I'll have something like a load method
        Essentially, this is a violation of the Single Responsibility Principle, because what essentially happened
        is that the entries of the journal have taken a new concert. We now have the journal not only handling entries,
        it is also handling persistence - this is a separate concern. We want to achieve separation of concerns,
        thus we need to move the methods below to a separate class. We want the journal to have a SINGLE responsibility.
        If you want persistence, you need to make a new class (as defined below my current Journal class)
    */
    public void load(String fileName) {
    }

    public void load(URL url) {
    }

}

class Persistence {
    // This is where you would put the persistence functionality (saving/loading files) and not just for the Journal class
    // but any additional classes that you might have in your application.

    public void saveToFile(Journal journal, String fileName, boolean overwrite) throws FileNotFoundException {
        if (overwrite || new File(fileName).exists()) {
            try (PrintStream out = new PrintStream(fileName)) {
                out.println(journal.toString());
            }
        }
    }
    public void load(){
        //logic
    }
}
